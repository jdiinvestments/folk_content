/*----------------------------------------------------------------------------
 File:           folk.content.slider.js
 Author:         John Tilley
 Company:        Folk Digital
 www.wearefolk.com
 Description:	 Apply a 360 slider to a group of features
 ----------------------------------------------------------------------------*/

;(function ( $ ) {
    $.fn.folkContentSlider = function( options ) {
        // These are the default options
        var settings = $.extend({}, $.fn.folkContentSlider.defaults, options );

        var animating = false;
        var currentSlide = 1;
        var hovered = false;
        var nextSlideLoop = null;
        var navButtonsArray = new Array();
        var $slides;

        return this.each(function() {
            var $slider = $( this );

            /*
             This metaphysical Ferry has been introduced because sometimes a gap appears
             between two moving slides. The idea is that two slides get moved onto the ferry,
             then the ferry moves across the screen. After the animation the slides are taken
             out and put back in the slider. This should prevent images drifting apart because
             they're now moving together in the same container. Is this efficient? I don't know.
             */
            var $slideFerry = $('<div/>').css({
                display: 'none',
                position: 'absolute',
                top: 0,
                left:0,
                zIndex: (sliderZindex+4)
            }).appendTo($slider);

            $slides = $slider.find(settings.slides);

            // Work out the base z-Index of the sliders
            var sliderZindex = $slider.css('z-index');
            if (isNaN(sliderZindex)) {
                sliderZindex = 0;
            } else {
                sliderZindex = parseInt(sliderZindex);
            }

            // Style slider
            $slider.css({
                overflow: 'hidden',
                position: 'relative'
            });

            // Style Slides
            $slides.css({
                width: '100%',
                height: 'auto',
                left: '100%',
                top: 0,
                display: 'block',
                overflow: 'hidden',
                position: 'absolute',
                zIndex: (sliderZindex+1)
            });

            // Make sure the first one is on top
            $slides.eq(0).css({
                left:0,
                zIndex: (sliderZindex+3)
            });

            // Resize the window to make sure all the slides fit
            $(window).resize(function () {
                var maxHeight = 0;
                $slides.each(function() {
                    maxHeight = $(this).height() > maxHeight ? $(this).height() : maxHeight;
                });
                $slider.height(maxHeight);
            }).resize();

            $(window).load(function () { $(window).resize(); });

            // If multiple slides add arrows and sliding functionality
            if ($slides.length > 1)
            {

                // Create and style the arrows
                // Left
                var leftArrowLoad = new Image();
                leftArrowLoad.src = settings.arrowLeftImage;

                var leftArrow = $('<img />');
                leftArrowLoad.onload = function () {
                    leftArrow
                        .attr('src', settings.arrowLeftImage)
                        .addClass(settings.arrowClass)
                        .css({
                            position: 'absolute',
                            top:'50%',
                            left: '1%',
                            zIndex: (sliderZindex+4),
                            cursor: 'pointer'
                        })
                        .appendTo($slider);
                    leftArrow.css('margin-top', -(leftArrow.height() / 2));
                }

                // Right
                var rightArrowLoad = new Image();
                rightArrowLoad.src = settings.arrowRightImage;

                var rightArrow = $('<img />');
                rightArrowLoad.onload = function () {
                    rightArrow
                        .attr('src', settings.arrowRightImage)
                        .addClass(settings.arrowClass)
                        .css({
                            position: 'absolute',
                            top:'50%',
                            right: '1%',
                            zIndex: (sliderZindex+4),
                            cursor: 'pointer'
                        })
                        .appendTo($slider);
                    rightArrow.css('margin-top', -(rightArrow.height() / 2));
                }

                // Then correct the margins
                rightArrow.css('margin-top', -(rightArrow.height() / 2));

                // Create the slider navigation
                var sliderNav = $('<div />');
                for (var i = 1; i <= $slides.length; i++) {
                    var sliderNavButton = $('<div />');
                    sliderNavButton
                        .addClass('folk-content-nav-button')
                        .appendTo(sliderNav)
                        .attr('data-slide', i)
                        .css({
                            zIndex: (sliderZindex+4)
                        });
                    navButtonsArray.push(sliderNavButton);
                    sliderNavButton.on('click', function (e) {
                        goToslide($(this).data('slide'));
                    });
                }
                sliderNav
                    .addClass('folk-content-nav')
                    .appendTo($slider)
                    .css({
                        zIndex: (sliderZindex+4),
                        marginLeft: -(sliderNav.width() / 2)
                    });

                /*******************
                 * Set up the Events
                 */

                leftArrow.on('click touchstart', function (e) {
                    prevSlide();
                });

                rightArrow.on('click touchstart', function (e) {
                    nextSlide();
                });

                // When the mouse is over the slider it will pause
                $slider.on('mouseenter', function (e) {
                    hovered = true;
                    if (settings.pauseOnHover) { clearTimeout(nextSlideLoop); }
                }).on('mouseleave', function (e) {
                    hovered = false;
                    if (settings.pauseOnHover) { setLoop(); }
                });

                // Start Auto Looping
                setLoop();
                markAsActive();
            }

            /*----------------------------------------------------------------------------
             Start Auto Loop
             ----------------------------------------------------------------------------*/
            function setLoop()
            {
                clearTimeout(nextSlideLoop);
                if (settings.auto && (!settings.pauseOnHover || !hovered)) {
                    nextSlideLoop = setTimeout(function () { nextSlide() }, settings.delay);
                } else {
                    nextSlideLoop = setTimeout(function () { setLoop() }, settings.delay);
                }
            }

            /*----------------------------------------------------------------------------
             Go to next slide
             ----------------------------------------------------------------------------*/
            function nextSlide()
            {
                if (!animating && $slides.length > 1) {
                    var previousSlide = currentSlide;
                    currentSlide += 1;
                    if (currentSlide > $slides.length) {
                        currentSlide = 1;
                    }
                    slideLeft(previousSlide, currentSlide);
                }
                setLoop();
            }

            /*----------------------------------------------------------------------------
             Go to previous slide
             ----------------------------------------------------------------------------*/
            function prevSlide()
            {
                if (!animating && $slides.length > 1) {
                    var previousSlide = currentSlide;
                    currentSlide -= 1;
                    if (currentSlide < 1) {
                        currentSlide = $slides.length;
                    }
                    slideRight(previousSlide, currentSlide);
                }
                setLoop();
            }
            /*----------------------------------------------------------------------------
             Slides a previous and next slide from right to left
             ----------------------------------------------------------------------------*/
            function slideLeft(prev, current, callback)
            {
                if (!animating) {
                    animating = true;

                    // Convert parameters to indexes
                    prev--;
                    current--;

                    $slideFerry
                        .css({
                            width: '200%',
                            height: '100%',
                            left: 0
                        })
                        .append($slides.eq(prev).css({ left:'0%', width: '50%', zIndex: (sliderZindex+3) }))
                        .append($slides.eq(current).css({ left:'50%', width: '50%', zIndex: (sliderZindex+2) }))
                        .show()
                        .animate({ left: '-100%' }, options.speed, options.easing, function () {
                            $slider
                                .append($slides.eq(prev).css({ left:'0%', width: '100%', zIndex: (sliderZindex+1) }))
                                .append($slides.eq(current).css({ left:'0%', width: '100%', zIndex: (sliderZindex+3) }));
                            animating = false;
                            $slideFerry.hide();
                            if (callback && typeof(callback) === "function") {
                                callback();
                            }
                        });

                    setTimeout(markAsActive, options.speed/2);
                }
            }

            /*----------------------------------------------------------------------------
             Slides a previous and next slide from left to right
             ----------------------------------------------------------------------------*/
            function slideRight(prev, current, callback)
            {
                if (!animating) {
                    animating = true;

                    // Convert parameters to indexes
                    prev--;
                    current--;

                    $slideFerry
                        .css({
                            width: '200%',
                            height: '100%',
                            left: '-100%'
                        })
                        .append($slides.eq(prev).css({ left:'50%', width: '50%', zIndex: (sliderZindex+3) }))
                        .append($slides.eq(current).css({ left:'0%', width: '50%', zIndex: (sliderZindex+2) }))
                        .show()
                        .animate({ left: '0%' }, options.speed, options.easing, function () {
                            $slider
                                .append($slides.eq(prev).css({ left:'0%', width: '100%', zIndex: (sliderZindex+1) }))
                                .append($slides.eq(current).css({ left:'0%', width: '100%', zIndex: (sliderZindex+3) }));
                            animating = false;
                            $slideFerry.hide();
                            if (callback && typeof(callback) === "function") {
                                callback();
                            }
                        });

                    setTimeout(markAsActive, options.speed/2);
                }
            }


            /*----------------------------------------------------------------------------
             Works out which slide to go to and then calls a recursive slider function
             ----------------------------------------------------------------------------*/

            function goToslide(slide_number) {
                if (!animating && slide_number > 0)
                {
                    // Create two arrays, going each way
                    var prev_array = new Array();
                    var prev = currentSlide;
                    while(prev != slide_number) {
                        prev_array.push(prev);
                        prev--;
                        if (prev < 1) { prev = $slides.length; }
                        if (prev == currentSlide) { break; }
                    }
                    prev_array.push(slide_number);

                    var next_array = new Array();
                    var next = currentSlide;
                    while(next != slide_number) {
                        next_array.push(next);
                        next++;
                        if (next > $slides.length) { next = 1; }
                        if (next == currentSlide) { break; }
                    }
                    next_array.push(slide_number);

                    // Determine direction and trigger event
                    if (prev_array.length < next_array.length) { recursiveSlideRight(prev_array); }
                    else { recursiveSlideLeft(next_array); }
                    currentSlide = slide_number;
                }
                setLoop();
            }

            /*----------------------------------------------------------------------------
             Takes an array of slides and animates them all in one go
             ----------------------------------------------------------------------------*/
            function recursiveSlideLeft(next_array) {
                if (!animating && next_array.length > 1) {
                    animating = true;

                    var startpoint = 0;
                    var endpoint = -100 * (next_array.length - 1);

                    for (var i=0; i < next_array.length; i++) {
                        var div_id = next_array[i]-1;

                        // Change the zindexes
                        if (i == 0) {
                            $slides.eq(div_id).css({zIndex:sliderZindex+3});
                        } else {
                            $slides.eq(div_id).css({zIndex:sliderZindex+2});
                        }

                        // Set up the start point of the animation
                        $slides.eq(div_id).animate({top:0, left: startpoint+"%"}, 0);

                        // Set up the enpoint and if it's the last item in the array then set a call back function for the end of the animation
                        if (i != next_array.length - 1) {
                            $slides.eq(div_id).animate({top:0, left: endpoint+"%"}, options.speed, options.easing);
                        } else {
                            $slides.eq(div_id).animate({top:0, left: endpoint+"%"}, options.speed, options.easing, function() {
                                $slides.css({zIndex:sliderZindex+1});
                                $slides.eq(div_id).css({zIndex:sliderZindex+3});
                                animating = false;
                            });
                        }
                        startpoint += 100;
                        endpoint += 100;

                        if (i > 0) {
                            setTimeout(
                                markAsActive,
                                (i / (next_array.length-1) * options.speed) - (options.speed/(next_array.length-1)/2),
                                div_id+1
                            );
                        }
                    }
                }
            }

            /*----------------------------------------------------------------------------
             Takes an array of slides and animates them all in one go
             ----------------------------------------------------------------------------*/
            function recursiveSlideRight(prev_array) {
                if (!animating && prev_array.length > 1) {
                    animating = true;

                    var startpoint = 0;
                    var endpoint = 100 * (prev_array.length - 1);

                    for (var i=0; i < prev_array.length; i++) {
                        var div_id = prev_array[i]-1;

                        // Change the zindexes
                        if (i == 0) {
                            $slides.eq(div_id).css({zIndex:sliderZindex+3});
                        } else {
                            $slides.eq(div_id).css({zIndex:sliderZindex+2});
                        }

                        // Set up the start point of the animation
                        $slides.eq(div_id).animate({top:0, left: startpoint+"%"}, 0);

                        // Set up the enpoint and if it's the last item in the array then set a call back function for the end of the animation
                        if (i != prev_array.length - 1) {
                            $slides.eq(div_id).animate({top:0, left: endpoint+"%"}, options.speed, options.easing);
                        } else {
                            $slides.eq(div_id).animate({top:0, left: endpoint+"%"}, options.speed, options.easing, function() {
                                $slides.css({zIndex:sliderZindex+1});
                                $slides.eq(div_id).css({zIndex:sliderZindex+3});
                                animating = false;
                            });
                        }
                        startpoint -= 100;
                        endpoint -= 100;

                        if (i > 0) {
                            setTimeout(
                                markAsActive,
                                (i / (prev_array.length-1) * options.speed) - (options.speed/(prev_array.length-1)/2),
                                div_id+1
                            );
                        }
                    }
                }
            }

            /*----------------------------------------------------------------------------
             Marks the nav buttons as active
             ----------------------------------------------------------------------------*/
            function markAsActive(marked_number) {
                if (marked_number == null) {
                    marked_number = currentSlide;
                }
                for (var i = 0; i < navButtonsArray.length; i++) {
                    if (navButtonsArray[i].data('slide') == marked_number) {
                        navButtonsArray[i].addClass('active');
                    } else {
                        navButtonsArray[i].removeClass('active');
                    }
                }
            }
        });
    };

    $.fn.folkContentSlider.defaults = {
        slides: null,
        arrowClass: 'arrow',
        arrowLeftImage: null,
        arrowRightImage: null,
        pauseOnHover: true,
        easing: 'easeInOutQuart',
        auto: true,
        speed: 500,                 //  The time taken to complate a slide animation (in milliseconds)
        delay: 500                 //  The delay between slide animations (in milliseconds)
    };

}( jQuery ));