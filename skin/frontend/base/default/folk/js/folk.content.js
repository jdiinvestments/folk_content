/*----------------------------------------------------------------------------
 File:           folk.content.js
 Author:         John Tilley
 Company:        Folk Digital
 www.wearefolk.com
 Description:	 Controls elements on a folk content
 ----------------------------------------------------------------------------*/

;(function ( $ ) {
    $.fn.folkContent = function( options ) {
        // These are the default options
        var settings = $.extend({}, $.fn.folkContent.defaults, options );

        return this.each(function() {
            var $this = $( this );
            var $container = $this.find('.product-markers');
            var $infoWindow = $this.find('.info-window');
            var markerTimeout;

            // If there are markers
            if (settings.markers != null)
            {
                var markersImageLoad = new Image();
                markersImageLoad.src = settings.markerImage;
                markersImageLoad.onload = initMarkers;
            }

            jQuery(window).resize(function () {
                resizeText();
            }).resize();

            $this.on('click', function (e) {
                mainFeatureUrl (e);
            });

            // Enable Responsive Videos
            $this.find('.feature-video').fitVids();


            /* Private Functions */
            function resizeText() {
                var layerWidth = $this.width();
                var percentageWidth = (layerWidth / 1000).toFixed(3);
                $this.find(".items").css("font-size", percentageWidth+"em");
            }

            function mainFeatureUrl (e) {
                var target = jQuery(e.target);
                if (!target.is(jQuery("input.link-add-to-cart.button")) && !target.parents(".info-window").length && settings.imageUrl != '')
                {
                    window.location = settings.imageUrl;
                }
            }

            function initMarkers() {
                // Add the markers
                for (var key in settings.markers) {
                    if (settings.markers.hasOwnProperty(key)) {
                        var markerData = settings.markers[key];
                        addMarker(markerData);
                    }
                }

                // Now add events to those markers
                var allMarkers = $container.find('.product-marker');

                allMarkers.on("mouseenter touchstart", function (e) {
                    clearTimeout(markerTimeout);
                    populateInfoWindow(this);

                });
                allMarkers.on("mouseleave", function (e) {
                    markerTimeout = setTimeout(hideInfoWindow,500);
                });

                $infoWindow.on("mouseleave", function(e){
                    overWindow = false;
                    markerTimeout = setTimeout(hideInfoWindow,500);
                    e.stopPropagation();
                });

                $infoWindow.on("mouseenter touchstart", function(e){
                    clearTimeout(markerTimeout);
                    overWindow = true;
                    e.stopPropagation();
                });
            }


            // Adds a marker to the container
            function addMarker(data)
            {
                var marker;

                marker = jQuery('<span />')
                    // Label
                    .attr('rel', data.item_id)
                    .attr('title', data.label)
                    .data('product-id',data.product_id)
                    .data('item-id',data.item_id)
                    // Fixed values
                    .addClass('product-marker')
                    .css('height','auto')
                    .css('position','absolute')
                    .css('line-height',0)
                    .css('display','block')
                    .css('background-repeat', 'no-repeat');

                marker.appendTo($container);

                var img = jQuery('<img />')
                    .css('width','100%')
                    .css('height','auto')
                    .css('vertical-align','bottom')
                    .attr('src',settings.markerImage)
                    .appendTo(marker);

                marker
                    // Positioning
                    .css('left', data.coord_x+"%")
                    .css('top', data.coord_y+"%")
                    .css('margin-left', -(img.width() / 2))
                    .css('margin-top', -(img.height() / 2));
            }

            // Hides all info-windows unless it is currently viewed.
            function hideInfoWindow()
            {
                $container.find('.product-marker').css({zIndex: 51});
                jQuery('.info-window').each(function() {
                    jQuery(this).hide();
                });
                jQuery('.info-window').html('');
            }


            // Adds the text to the info-window
            function populateInfoWindow(element)
            {
                hideInfoWindow();
                $infoWindow.html('');

                var $element = jQuery(element);
                var item_id = $element.attr('rel');

                $element.css({zIndex: 60});

                $infoWindow.html(settings.markers[item_id]['info_window_html']);
                $infoWindow.show();

                var pos = getInfoPosition($element);

                $infoWindow.css('left', pos.left)
                    .css('top', pos.top)
                    .css('right', pos.right)
                    .css('bottom', pos.bottom);
            }

            function getInfoPosition($element)
            {
                var left = 'auto', top = 'auto', right = 'auto', bottom = 'auto';
                if($element) {
                    var currentPosition = $element.position();
                    var markerX = currentPosition.left;
                    var markerY = currentPosition.top;

                    // Determine Horizontal position
                    var popupWidth = $infoWindow.outerWidth();
                    var popupHeight = $infoWindow.outerHeight();

                    if (markerX + popupWidth < $container.width()) {
                        left = markerX + settings.distanceFromMarker;
                    } else if (markerX - popupWidth > 0) {
                        right = $container.width() - markerX + settings.distanceFromMarker;
                    } else {
                        left = 0;
                    }

                    // Determine Vertical Position
                    if (markerY + popupHeight < $container.height()) {
                        top = markerY + settings.distanceFromMarker;
                    } else if (markerY - popupHeight > 0) {
                        bottom = $container.height() - markerY + settings.distanceFromMarker;
                    } else {
                        top = 0;
                    }
                }

                return {
                    left: left,
                    top: top,
                    right: right,
                    bottom: bottom
                }
            }
        });
    };

    $.fn.folkContent.defaults = {
        featureId: null,
        markers: null,
        imageUrl: null,
        mediaUrl: null,
        markerImage: '',
        distanceFromMarker: 0
    };

}( jQuery ));