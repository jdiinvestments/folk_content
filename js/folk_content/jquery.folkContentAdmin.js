/*----------------------------------------------------------------------------
 File:           jquery.folkContent.js
 Author:         John Tilley
 Company:        Folk Digital
 www.wearefolk.com
 Description:	Controls all the overlay elements on the folk content page
 ----------------------------------------------------------------------------*/

;(function ( $ ) {

    $.fn.folkContentAdmin = function( options ) {
        // These are the default options
        var settings = $.extend({}, $.fn.folkContentAdmin.defaults, options );

        return this.each(function() {
            var $this = $( this );

            // If the right settings
            if (settings.editor != null && settings.image != null)
            {
                // Make sure the editor image and table are entered as ids
                if (settings.editor.charAt(0) != '#') { settings.editor = "#"+settings.editor; }
                if (settings.image.charAt(0) != '#') { settings.image = "#"+settings.image; }
                if (settings.table.charAt(0) != '#') { settings.table = "#"+settings.table; }

                jQuery(".tab-item-link").on('click', function () {
                    updatePoints();
                    applyEvents();
                });
                var markersImageLoad = new Image();
                markersImageLoad.src = settings.markerImage;
                markersImageLoad.onload = init;
            }

            /* INIT CONSTRUCTOR*/
            function init()
            {
                updatePoints();
                applyEvents();

                /* Key Presses */
                settings.shiftPressed = false;
                jQuery(window).on('keydown', function (e) {
                    switch(e.keyCode) {
                        case 16:
                            settings.shiftPressed = true;
                            break;
                        case 27:
                            selectItem(null);
                            break;
                    }
                }).on('keyup', function (e) {
                        switch(e.keyCode) {
                            case 16:
                                settings.shiftPressed = false;
                                break;
                        }
                    });

                jQuery(settings.editor).on('click mousedown', function (e) {
                    if (!jQuery(e.target).hasClass('item-point') && !jQuery(e.target).parents('.item-point').length) {
                        selectItem(null);
                    }
                });
            }

            /* Private Functions */

            // Align Functions
            function alignBottom()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the bottom Most
                    var bottomMost = 0;
                    selectedItems.each(function () {
                        if (jQuery(this).position().top + jQuery(this).height() > bottomMost) { bottomMost = jQuery(this).position().top + jQuery(this).height(); }
                    });

                    // Set the bottom Most
                    selectedItems.each(function () { jQuery(this).css('top', bottomMost - jQuery(this).height()); });
                }
                else
                {
                    selectedItems.each(function () { jQuery(this).css('top', jQuery(settings.editor).height() - jQuery(this).height()); });
                }
                savePositions();
            }

            function alignCenter()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the left Most AND the bottom most
                    var leftMost = jQuery(settings.editor).width();
                    selectedItems.each(function () { if (jQuery(this).position().left < leftMost) { leftMost = jQuery(this).position().left; } });
                    var rightMost = 0;
                    selectedItems.each(function () { if (jQuery(this).position().left + jQuery(this).width() > rightMost) { rightMost = jQuery(this).position().left + jQuery(this).width(); } });

                    var distance = rightMost - leftMost;
                    var midPoint = leftMost + (distance / 2);

                    // Set the middle point Most
                    selectedItems.each(function () { jQuery(this).css('left', midPoint - (jQuery(this).width() / 2)); });
                }
                else
                {
                    selectedItems.each(function () { jQuery(this).css('left', (jQuery(settings.editor).width() / 2) - (jQuery(this).width() / 2)); });
                }
                savePositions();
            }

            function alignLeft()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the top Most
                    var leftMost = jQuery(settings.editor).width();
                    selectedItems.each(function () {
                        if (jQuery(this).position().left < leftMost) { leftMost = jQuery(this).position().left; }
                    });

                    // Set the top Most
                    selectedItems.each(function () { jQuery(this).css('left', leftMost); });
                }
                else
                {
                    selectedItems.each(function () { jQuery(this).css('left', 0); });
                }
                savePositions();
            }

            function alignMiddle()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the top Most AND the bottom most
                    var topMost = jQuery(settings.editor).height();
                    selectedItems.each(function () { if (jQuery(this).position().top < topMost) { topMost = jQuery(this).position().top; } });
                    var bottomMost = 0;
                    selectedItems.each(function () { if (jQuery(this).position().top + jQuery(this).height() > bottomMost) { bottomMost = jQuery(this).position().top + jQuery(this).height(); } });

                    var distance = bottomMost - topMost;
                    var midPoint = topMost + (distance / 2);

                    // Set the middle point Most
                    selectedItems.each(function () { jQuery(this).css('top', midPoint - (jQuery(this).height() / 2)); });
                }
                else
                {
                    selectedItems.each(function () { jQuery(this).css('top', (jQuery(settings.editor).height() / 2) - (jQuery(this).height() / 2)); });
                }
                savePositions();
            }

            function alignRight()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the bottom Most
                    var rightMost = 0;
                    selectedItems.each(function () {
                        if (jQuery(this).position().left + jQuery(this).width() > rightMost) { rightMost = jQuery(this).position().left + jQuery(this).width(); }
                    });

                    // Set the bottom Most
                    selectedItems.each(function () { jQuery(this).css('left', rightMost - jQuery(this).width()); });
                }
                else
                {
                    selectedItems.each(function () { jQuery(this).css('left', jQuery(settings.editor).width() - jQuery(this).width()); });
                }
                savePositions();
            }

            function alignTop()
            {
                var selectedItems = jQuery(settings.editor).find('.selected');
                if (selectedItems.length > 1)
                {
                    // Find the top Most
                    var topMost = jQuery(settings.editor).height();
                    selectedItems.each(function () {
                        if (jQuery(this).position().top < topMost) { topMost = jQuery(this).position().top; }
                    });

                    // Set the top Most
                    selectedItems.each(function () { jQuery(this).css('top', topMost); });
                }
                else
                {
                    // Set to top of window
                    selectedItems.each(function () { jQuery(this).css('top', 0); });
                }
                savePositions();
            }

            function applyEvents()
            {
                jQuery(settings.editor).find(".item-point").tooltip({
                    track: true
                });

                jQuery(settings.editor).find(".item-point").draggable({
                    /*snap: '.item-point',*/
                    containment: 'parent',
                    drag: dragSelected,
                    stop: dragStop
                });

                jQuery(settings.editor).find(".item-point")
                    .on('mouseenter', function (e) {
                        jQuery(settings.table)
                            .find('.item-row-'+jQuery(this).attr('rel'))
                            .addClass('highlight');
                    })
                    .on('mouseleave', function (e) {
                        jQuery(settings.table)
                            .find('.item-row-'+jQuery(this).attr('rel'))
                            .removeClass('highlight');
                    })
                    .on('mousedown', function (e) {
                        selectItem(this);
                    });

                // Align Functions
                jQuery(settings.editor).siblings('.feature-controls').find('#align_top_button').on('click', alignTop);
                jQuery(settings.editor).siblings('.feature-controls').find('#align_middle_button').on('click', alignMiddle);
                jQuery(settings.editor).siblings('.feature-controls').find('#align_bottom_button').on('click', alignBottom);
                jQuery(settings.editor).siblings('.feature-controls').find('#align_left_button').on('click', alignLeft);
                jQuery(settings.editor).siblings('.feature-controls').find('#align_center_button').on('click', alignCenter);
                jQuery(settings.editor).siblings('.feature-controls').find('#align_right_button').on('click', alignRight);
            }

            function changeZindex(e, button, direction)
            {
                e.preventDefault();
                e.stopPropagation();
                var row = jQuery(button).val();
                var zIndex = jQuery('#zindex_field_'+ row).val();

                if (direction == '+') {
                    zIndex++;
                } else if (direction == '-') {
                    zIndex--;
                }
                jQuery('#zindex_field_'+ row).val(zIndex);
                updateZindex(row);
            }

            // Drags other selected items
            function dragSelected(event, ui)
            {
                var id = ui.helper.attr('rel');
                var selectedItems = jQuery(settings.editor).find('.selected');

                // Get the current and previous location of this element
                var currentLoc = $(this).position();
                var prevLoc = $(this).data('prevLoc');
                if (!prevLoc) { prevLoc = ui.originalPosition; }

                var offsetLeft = currentLoc.left-prevLoc.left;
                var offsetTop = currentLoc.top-prevLoc.top;

                selectedItems.each(function () {
                    if (id != $this.id) {
                        $(this).css({
                            left: $(this).position().left + offsetLeft,
                            top: $(this).position().top + offsetTop
                        });
                    }
                });

                $(this).data('prevLoc', currentLoc);
            }

            // Draggable and Resizable functions
            function dragStop(event, ui)
            {
                $(this).data('prevLoc', '');
                savePositions();
            }

            function removePoints()
            {
                jQuery(settings.editor).find(".item-point").remove();
            }

            function resizeOverlay(event, ui)
            {
                var id = ui.helper.attr('rel');
                var parent = jQuery(settings.table).find('tbody');
                var width = ( ui.size.width / ui.helper.parent().width() ) * 100;

                jQuery(ui.element).css('height', 'auto');

                // Round to 15 decimal places
                width = Math.round(width * 1000000000000000) / 1000000000000000;

                var overlay_type = parent.find('input[name*="['+id+'][overlay_type]"]').val();

                if (overlay_type == 'image')
                {
                    parent.find('input[name*="['+id+'][image_width]"]').val(width);
                }
                else if (overlay_type == 'text')
                {
                    parent.find('input[name*="['+id+'][text_width]"]').val(width);
                }
            }

            function savePositions()
            {
                jQuery(settings.editor).find('.item-point').each(function () {
                    var $this = $(this);

                    var id = $this.attr('rel');
                    var parent = jQuery(settings.table).find('tbody');

                    var overlay_type = parent.find('input[name*="['+id+'][overlay_type]"]').val();

                    // Top the element going off the left or right of the editing area
                    if ($this.position().left < 0) {
                        $this.css('left', 0);
                    } else if ($this.position().left > $this.parent().width() - $this.width()) {
                        $this.css('left', $this.parent().width() - $this.width());
                    }
                    // Top the element going off the top or bottom of the editing area
                    if ($this.position().top < 0) {
                        $this.css('top', 0);
                    } else if ($this.position().top > $this.parent().height() - $this.height()) {
                        $this.css('top', $this.parent().height() - $this.height());
                    }

                    // Work out the percentage
                    var left =  (($this.position().left / $this.parent().width()) * 100);
                    var top = (($this.position().top / $this.parent().height()) * 100);

                    if (overlay_type == 'product') {
                        left =  ((($this.position().left + ($this.width() / 2)) / $this.parent().width()) * 100);
                        top = ((($this.position().top + ($this.height() / 2)) / $this.parent().height()) * 100);
                    }

                    // Round to 15 decimal places
                    left = Math.round(left * 1000000000000000) / 1000000000000000;
                    top = Math.round(top * 1000000000000000) / 1000000000000000;

                    parent.find('input[name*="['+id+'][coord_x]"]').val(left);
                    parent.find('input[name*="['+id+'][coord_y]"]').val(top);
                });
            }

            function selectItem(item)
            {
                var selectedItems = jQuery(settings.editor).find('.selected');

                if (item)
                {
                    if (settings.shiftPressed)
                    {
                        jQuery(item).addClass('selected');
                        jQuery(settings.table)
                            .find('.item-row-'+jQuery(item).attr('rel'))
                            .addClass('selected');
                    }
                    else
                    {
                        if (!jQuery(item).hasClass('selected'))
                        {
                            // Unselect everything and then select the currently selected one
                            selectedItems.removeClass('selected');
                            jQuery(settings.table).find('tr').removeClass('selected');
                            jQuery(item).addClass('selected');
                            jQuery(settings.table)
                                .find('.item-row-'+jQuery(item).attr('rel'))
                                .addClass('selected');
                        }
                    }
                }
                else
                {
                    selectedItems.removeClass('selected');
                    jQuery(settings.table).find('tr').removeClass('selected');
                }
            }

            function updatePoints()
            {
                removePoints();

                jQuery(settings.table).find("tbody").find("tr").each(function (index, item) {
                    var overlay_type = jQuery(item).find('input[name*="overlay_type"]').val();
                    var item_object = null;


                    jQuery(item).find(".zindex_minus_button").off('click').on('click', function (e) {
                        changeZindex(e, this, '-');
                    });
                    jQuery(item).find(".zindex_plus_button").off('click').on('click', function (e) {
                        changeZindex(e, this, '+');
                    });
                    jQuery(item).find('input[name*="z_index"]').off('blur').on('blur', function (e) {
                        updateZindex(this.id.replace('zindex_field_', ''));
                    });
                    jQuery(item).find('input[name*="z_index"]').off('keydown').on('keydown', function (e) {
                        if (e.keyCode == 13) {
                            updateZindex(this.id.replace('zindex_field_', ''));
                        }
                    });

                    switch(overlay_type) {
                        case 'image':
                        case 'text':
                            item_object = {
                                'item_id':          jQuery(item).find('input[name*="item_id"]').val(),
                                'feature_id':       jQuery(item).find('input[name*="feature_id"]').val(),
                                'label':            jQuery(item).find('input[name*="label"]').val(),
                                'overlay_type':     jQuery(item).find('input[name*="overlay_type"]').val(),
                                'coord_x':          jQuery(item).find('input[name*="coord_x"]'),
                                'coord_y':          jQuery(item).find('input[name*="coord_y"]'),
                                'z_index':          jQuery(item).find('input[name*="z_index"]').val(),
                                'image_width':      jQuery(item).find('input[name*="image_width"]'),
                                'image_url':        jQuery(item).find('input[name*="image_url"]').val(),
                                'image_src':        jQuery(item).find('input[name*="image_src"]').val(),
                                'text_width':       jQuery(item).find('input[name*="text_width"]'),
                                'text_url':         jQuery(item).find('input[name*="text_url"]').val(),
                                'text_content':     jQuery(item).find('input[name*="text_content"]').val(),
                                'font_size':        jQuery(item).find('input[name*="font_size"]').val(),
                                'font_face':        jQuery(item).find('input[name*="font_face"]').val(),
                                'colour':           jQuery(item).find('input[name*="colour"]').val(),
                                'alignment':        jQuery(item).find('input[name*="alignment"]').val(),
                                'line_height':      jQuery(item).find('input[name*="line_height"]').val(),
                                'letter_spacing':   jQuery(item).find('input[name*="letter_spacing"]').val(),
                                'bold':             jQuery(item).find('input[name*="bold"]').val(),
                                'underlined':       jQuery(item).find('input[name*="underlined"]').val(),
                                'italic':           jQuery(item).find('input[name*="italic"]').val()
                            };
                            break;
                        case 'product':
                            item_object = {
                                'product_id':       jQuery(item).find('input[name*="product_id"]').val(),
                                'entity_id':        jQuery(item).find('input[name*="entity_id"]').val(),
                                'name':             jQuery(item).find('input[name*="name"]').val(),
                                'sku':              jQuery(item).find('input[name*="sku"]').val(),
                                'coord_x':          jQuery(item).find('input[name*="coord_x"]'),
                                'coord_y':          jQuery(item).find('input[name*="coord_y"]'),
                                'overlay_type':     jQuery(item).find('input[name*="overlay_type"]').val()
                            };
                            break;
                    }

                    if (item_object)
                    {
                        var marker;

                        /*****************************/
                        /* TEXT OVERLAYS             */
                        /*****************************/
                        var containerWidth = jQuery(settings.editor).width();
                        var containerHeight = jQuery(settings.editor).height();

                        if (item_object.overlay_type == 'text')
                        {
                            if (item_object.text_width.val() == 0) { item_object.text_width.val(25); }
                            if (item_object.coord_x.val() > 100 - item_object.text_width) { item_object.coord_x.val(100 - item_object.text_width); }
                            if (item_object.coord_y.val() > 100) { item_object.coord_y.val(100); }
                            if (item_object.letter_spacing != 'normal') { item_object.letter_spacing = item_object.letter_spacing+"px"; }
                            if (item_object.line_height != 'auto') { item_object.line_height = item_object.line_height+"em"; }

                            marker = jQuery('<span>'+item_object.text_content+'</span>')
                                // Label
                                .attr('rel', item_object.item_id)
                                .attr('title', item_object.label+' ('+item_object.item_id+')')
                                // Positioning
                                .css('left', (item_object.coord_x.val()/100)*containerWidth + 'px')
                                .css('top', (item_object.coord_y.val()/100)*containerHeight + 'px')
                                .css('width',item_object.text_width.val()+'%')
                                .css('height','auto')
                                // Styling
                                .css('font-size',item_object.font_size+'em')
                                .css('font-family',item_object.font_face)
                                .css('color',item_object.colour)
                                .css('text-align',item_object.alignment)
                                .css('line-height',item_object.line_height)
                                .css('letter-spacing',item_object.letter_spacing)
                                .css('z-index',item_object.z_index)

                                // Fixed values
                                .addClass('item-point')
                                .addClass('drag')
                                .attr('id', 'item-zindex-'+item_object.item_id)
                                .css('position','absolute')
                                .css('display','block')
                                .css('background-repeat', 'no-repeat');

                            console.log(item_object);

                            if (parseInt(item_object.bold)) { marker.css('font-weight', 'bold'); }
                            if (parseInt(item_object.underlined)) { marker.css('text-decoration', 'underline'); }
                            if (parseInt(item_object.italic)) { marker.css('font-style', 'italic'); }

                            marker.appendTo(settings.editor);


                            jQuery(marker).resizable({
                                containment: 'parent',
                                handles: "e",
                                stop: resizeOverlay
                            });
                        }
                        /*****************************/
                        /* IMAGE OVERLAYS            */
                        /*****************************/
                        else if (item_object.overlay_type == 'image')
                        {
                            if (item_object.image_width.val() == 0) { item_object.image_width.val(25); }
                            if (item_object.coord_x.val() > 100 - item_object.image_width) { item_object.coord_x.val(100 - item_object.image_width); }
                            if (item_object.coord_y.val() > 100) { item_object.coord_y.val(100); }
                            if (item_object.letter_spacing != 'normal') { item_object.letter_spacing = item_object.letter_spacing+"px"; }
                            if (item_object.line_height != 'auto') { item_object.line_height = item_object.line_height+"em"; }

                            marker = jQuery('<span />')
                                // Label
                                .attr('rel', item_object.item_id)
                                .attr('title', item_object.label+' ('+item_object.item_id+')')
                                // Positioning
                                .css('left', (item_object.coord_x.val()/100)*containerWidth + 'px')
                                .css('top', (item_object.coord_y.val()/100)*containerHeight + 'px')
                                .css('width',item_object.image_width.val()+'%')
                                .css('height','auto')
                                // Styling
                                .css('z-index',item_object.z_index)
                                // Fixed values
                                .addClass('item-point')
                                .addClass('drag')
                                .attr('id', 'item-zindex-'+item_object.item_id)
                                .css('position','absolute')
                                .css('line-height',0)
                                .css('display','block')
                                .css('background-repeat', 'no-repeat');

                            marker.appendTo(settings.editor);

                            var img = jQuery('<img />')
                                .css('width','100%')
                                .css('height','auto')
                                .css('vertical-align','bottom')
                                .attr('src',settings.mediaUrl+item_object.image_src)
                                .appendTo(marker);


                            jQuery(marker).resizable({
                                containment: 'parent',
                                handles: "e",
                                aspectRatio: true,
                                stop: resizeOverlay
                            });
                        }
                        /*****************************/
                        /* PRODUCT OVERLAYS          */
                        /*****************************/
                        else if (item_object.overlay_type == 'product')
                        {
                            marker = jQuery('<span />')
                                // Label
                                .attr('rel', item_object.product_id)
                                .attr('title', item_object.name+' ('+item_object.product_id+')')
                                // Positioning
                                .css('height','auto')
                                // Fixed values
                                .addClass('item-point')
                                .addClass('drag')
                                .css('position','absolute')
                                .css('line-height',0)
                                .css('display','block')
                                .css('background-repeat', 'no-repeat');

                            marker.appendTo(settings.editor);

                            var img = jQuery('<img />')
                                .css('width','100%')
                                .css('height','auto')
                                .css('vertical-align','bottom')
                                .attr('src',settings.markerImage)
                                .appendTo(marker);


                            if (item_object.coord_x.val() > 100) { item_object.coord_x.val(100); }
                            if (item_object.coord_y.val() > 100) { item_object.coord_y.val(100); }

                            var product_x = ((item_object.coord_x.val()/100)*containerWidth) - (img.width() / 2) + 'px';
                            var product_y = ((item_object.coord_y.val()/100)*containerHeight) - (img.height() / 2) + 'px';

                            marker
                                .css('left', product_x)
                                .css('top', product_y);
                        }
                    }
                });
            }

            function updateZindex(row)
            {
                var zIndex = jQuery('#zindex_field_'+ row).val();
                if (isNaN(zIndex)) {
                    zIndex = 0;
                } else {
                    if (zIndex < 0) {
                        zIndex = 0;
                    } else if (zIndex > 50) {
                        zIndex = 50;
                    }
                }
                jQuery('#zindex_field_'+ row).val(zIndex);
                jQuery('#item-zindex-'+ row).css('z-index', zIndex);
            }

        });
    };

    $.fn.folkContentAdmin.defaults = {
        editor: null,
        image: null,
        table: null,
        mediaUrl: '',
        markerImage: ''
    };

}( jQuery ));