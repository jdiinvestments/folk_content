 <?php

class Folk_Content_Block_Adminhtml_Feature_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /* Initialize grid settings */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('feature_list');
        $this->setDefaultLimit(200);
        $this->setDefaultSort('feature_id');
        $this->setDefaultDir('ASC');

        /* Override method getGridUrl() in this class to provide URL for ajax */
        $this->setUseAjax(true);
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * Prepare distributor collection
     *
     * @return Folk_Content_Block_Adminhtml_Features_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('folk_content/feature_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('feature_id', array(
            'header' => $this->__('ID'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'feature_id'
        ));

        $this->addColumn('title', array(
            'header' => $this->__('Title'),
            'sortable' => true,
            'index' => 'title',
            'column_css_class' => 'title'
        ));

        $this->addColumn('enabled', array(
            'header' => $this->__('Enabled'),
            'index' => 'enabled',
            'type'  => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled'
            )
        ));

        $this->addColumn('action', array(
            'header' => $this->__('Action'),
            'width' => '100px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => $this->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id',
                ),
                array(
                    'caption' => $this->__('Delete'),
                    'url' => array('base' => '*/*/delete'),
                    'field' => 'id',
                ),

            ),
            'filter' => false,
            'sortable' => false,
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('Layouts');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/deleteMass')
        ));

        return parent::_prepareMassaction();
    }
}