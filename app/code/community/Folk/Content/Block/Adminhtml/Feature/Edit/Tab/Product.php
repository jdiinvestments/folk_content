<?php

class Folk_Content_Block_Adminhtml_Feature_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('feature_product_grid');
        $this->setDefaultLimit(200);
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('ASC');
        //$this->setDefaultFilter(array('in_products' => 1));
        $this->setUseAjax(true);
        $this->setTemplate('folk/content/widget/grid.phtml');
    }

    /**
     * Add filter
     *
     * @param object $column
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Related
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id',
                    array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id',
                        array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**/
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection
            ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addFieldToFilter('visibility', array('neq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE ))
            ->addAttributeToSelect('name');

        // Join the two tables together
        if($this->getRequest()->getParam('id')) {

            $itemCollection = Mage::helper('folk_content')->getFeatureItemCollection($this->getRequest()->getParam('id'));
            $itemCollection->addFieldToFilter('overlay_type', array('in' => array('product')));

            $itemCollection->getSelect()
                ->joinLeft(
                    array('item_product' => $itemCollection->getTable('folk_content/itemproduct')),
                    'main_table.item_id = item_product.item_id',
                    array( // Don't include item_id
                        'item_product_id',
                        'product_id'
                    )
                );

            $collection->getSelect()
                ->joinLeft(
                    array('item_product_table' => new Zend_Db_Expr('('.$itemCollection->getSelect().')')),
                    'e.entity_id = item_product_table.product_id'
                );

            // This line sorts everything by product_id, but only if it's populated. Therefore bring selected items to the top
            $collection->getSelect()->order(array('IF(`product_id`>0,`product_id`,9999) ASC'));

            $this->setCollection($collection);
        }

        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('product_id',
            array(
                'header_css_class'  => 'a-center',
                'type'              => 'checkbox',
                'align'             => 'center',
                'values'            => $this->_getSelectedProducts(),
                'index'             => 'entity_id',
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_checkbox',
                'header'            => '&nbsp',
                'sortable'          => false,
                'name'              => 'product_id',
                'field_name'        => 'product_id',
                'field_name_group'  => 'item_product'
            ));

        $this->addColumn('entity_id',
            array(
                'header'            => Mage::helper('catalog')->__('ID'),
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
                'sortable'          => true,
                'width'             => 60,
                'index'             => 'entity_id',
                'name'              => 'entity_id',
                'field_name'        => 'entity_id',
                'field_name_group'  => 'item_product'
            ));

        $this->addColumn('name',
            array(
                'header'            => Mage::helper('catalog')->__('Name'),
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
                'sortable'          => true,
                'index'             => 'name',
                'name'              => 'name',
                'field_name'        => 'name',
                'field_name_group'  => 'item_product'
            ));

        $this->addColumn('sku',
            array(
                'header'            => Mage::helper('catalog')->__('SKU'),
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
                'sortable'          => true,
                'width'             => 150,
                'index'             => 'sku',
                'name'              => 'sku',
                'field_name'        => 'sku',
                'field_name_group'  => 'item_product',
                'index'             => 'sku'
            ));

        $this->addColumn('coord_x',
            array(
                'header'            => Mage::helper('catalog')->__('X Coordinates (%)'),
                'type'              => 'number',
                'validate_class'    => 'validate-number',
                'filter'            => false,
                'index'             => 'coord_x',
                'width'             => 50,
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
                'column_css_class'  => 'no-display',
                'header_css_class'  => 'no-display',
                'name'              => 'coord_x',
                'field_name'        => 'coord_x',
                'field_name_group'  => 'item_product'
            ));


        $this->addColumn('coord_y',
            array(
                'header'            => Mage::helper('catalog')->__('Y Coordinates (%)'),
                'type'              => 'number',
                'validate_class'    => 'validate-number',
                'filter'            => false,
                'index'             => 'coord_y',
                'width'             => 50,
                'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
                'column_css_class'  => 'no-display',
                'header_css_class'  => 'no-display',
                'name'              => 'coord_y',
                'field_name'        => 'coord_y',
                'field_name_group'  => 'item_product'
            ));

        $this->addColumn('overlay_type', array(
            'header'                => $this->__('Overlay Type'),
            'renderer'              => 'folk_content/adminhtml_widget_grid_column_renderer_label',
            'index'                 => 'overlay_type',
            'type'                  => 'options',
            'options'               => array(
                'text'                  => 'Text',
                'image'                 => 'Image',
                'product'               => 'Product'
            ),
            'column_css_class'      => 'overlay_type no-display',
            'header_css_class'      => 'no-display',
            'name'                  => 'overlay_type',
            'field_name'            => 'overlay_type',
            'field_name_group'      => 'item_product'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/productGrid',
            array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return null;
    }

    protected function _getSelectedProducts()
    {
        $id = $this->getEntityId() ? $this->getEntityId() : $this->getRequest()->getParam('id');

        $selectedCollection = Mage::getModel('folk_content/item')->getCollection()->addFieldToFilter('feature_id', $id);
        $selectedCollection->getSelect()
            ->joinLeft(
                array('item_product' => $selectedCollection->getTable('folk_content/itemproduct')),
                'main_table.item_id = item_product.item_id'
            );

        return array_keys($this->_getSelectedProductsArray($selectedCollection));
    }

    protected function _getSelectedProductsArray($products)
    {
        $selectedProducts = array();
        foreach ($products as $product) {
            $selectedProducts[$product->getProductId()] = $product->getSku();
        }
        return $selectedProducts;
    }

    protected function _getFeatureObject()
    {
        return Mage::getModel('home/features')->load($this->getRequest()->getParam('id'));
    }


    public function displayCollection()
    {
        return $this->_getFeatureObject()->getSelectedLayers();
    }

}
