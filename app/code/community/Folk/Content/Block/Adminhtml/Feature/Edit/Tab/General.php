<?php

class Folk_Content_Block_Adminhtml_Feature_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Add the form fields
     *
     * @return Folk_Home_Block_Adminhtml_Features_Edit_Tab_General
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('general');

        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $this->__('General Setup')
        ));

        $fieldset->addField('title', 'text', array(
            'label' => $this->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('enabled', 'select', array(
            'label' => 'Enabled',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'enabled',
            'value' => 1,
            'values' => array(
                array('value' => 0, 'label' => 'No'),
                array('value' => 1, 'label' => 'Yes')
            )
        ));


        $fieldset->addField('sort_order', 'text',
            array(
                'label' => 'Sort Order',
                'class' => 'required-entry validate-number',
                'required' => true,
                'value' => 0,
                'name' => 'sort_order'
            ));

        $fieldset->addField('link_url', 'text', array(
            'label' => $this->__('Link URL'),
            'class' => '',
            'required' => false,
            'name' => 'link_url',
        ));


        $fieldset->addField('bg_image', 'image', array(
            'label' => $this->__('Background Image'),
            'class' => '',
            'required' => false,
            'name' => 'feature[bg_image]',
        ));

        $fieldset->addField('mobile_image', 'image', array(
            'label' => $this->__('Mobile Image'),
            'class' => '',
            'required' => false,
            'name' => 'feature[mobile_image]',
        ));

        $fieldset->addField('embed_script', 'textarea', array(
            'label' => 'Video Embed Code',
            'after_element_html' => 'Note: Including a video will override the background image',
            'name' => 'embed_script'
        ));

        $form->addValues($this->_getFormData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     *
     * @return array
     */
    protected function _getFormData()
    {
        $data = Mage::getSingleton('adminhtml/session')->getFormData();

        if (Mage::registry('current_feature')->getData()) {
            $data = Mage::registry('current_feature')->getData();
        }

        return (array)$data;
    }
}