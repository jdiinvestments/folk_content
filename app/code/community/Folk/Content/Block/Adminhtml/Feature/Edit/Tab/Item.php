<?php

class Folk_Content_Block_Adminhtml_Feature_Edit_Tab_Item extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('feature_item_grid');
        $this->setDefaultLimit(200);
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setTemplate('folk/content/widget/grid.phtml');
    }

    protected function _prepareLayout()
    {
        $icons_url = Mage::getBaseUrl('js') . 'folk_content/icons/';
        $featureId = Mage::helper('folk_content')->getCurrentFeatureId();

        /* ADD BUTTONS */
        $this->setChild('add_text_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'add-text-icon.svg" />',
                    'id'        => 'add_text_overlay_button',
                    'onclick'   => 'window.location.href=\''.$this->getUrl('*/item/edittext/feature/' . $featureId).'\'',
                    'class'     => 'task',
                    'title'     => 'Add Text'
                ))
        );
        $this->setChild('add_image_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'add-image-icon.svg" />',
                    'id'        => 'add_image_overlay_button',
                    'onclick'   => 'window.location.href=\''.$this->getUrl('*/item/editimage/feature/' . $featureId).'\'',
                    'class'     => 'task',
                    'title'     => 'Add Image'
                ))
        );

        /* ALIGN BUTTONS */
        $this->setChild('align_top_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-top-edge.svg" />',
                    'id'        => 'align_top_button',
                    'class'     => 'task',
                    'title'     => 'Align Top Edge'
                ))
        );
        $this->setChild('align_middle_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-middle.svg" />',
                    'id'        => 'align_middle_button',
                    'class'     => 'task',
                    'title'     => 'Align Vertical Centers'
                ))
        );
        $this->setChild('align_bottom_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-bottom-edge.svg" />',
                    'id'        => 'align_bottom_button',
                    'class'     => 'task',
                    'title'     => 'Align Bottom Edge'
                ))
        );
        $this->setChild('align_left_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-left-edge.svg" />',
                    'id'        => 'align_left_button',
                    'class'     => 'task',
                    'title'     => 'Align Left Edge'
                ))
        );
        $this->setChild('align_center_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-center.svg" />',
                    'id'        => 'align_center_button',
                    'class'     => 'task',
                    'title'     => 'Align Horizontal Centers'
                ))
        );
        $this->setChild('align_right_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => '<img src="'.$icons_url.'align-right-edge.svg" />',
                    'id'        => 'align_right_button',
                    'class'     => 'task',
                    'title'     => 'Align Right Edge'
                ))
        );

        return parent::_prepareLayout();
    }


    public function getMainButtonsHtml()
    {
        $html = '';
        //$html .= parent::getMainButtonsHtml();
        /*if($this->getFilterVisibility()){
            $html.= $this->getChildHtml('add_text_overlay_button');
            $html.= $this->getChildHtml('add_image_overlay_button');
        }*/
        return $html;
    }

    public function getFeatureAddButtonsHtml()
    {
        $html = '';
        $html.= $this->getChildHtml('add_text_button');
        $html.= $this->getChildHtml('add_image_button');
        return $html;
    }

    public function getFeatureAlignButtonsHtml()
    {
        $html = '';
        $html.= $this->getChildHtml('align_top_button');
        $html.= $this->getChildHtml('align_middle_button');
        $html.= $this->getChildHtml('align_bottom_button');
        $html.= $this->getChildHtml('align_left_button');
        $html.= $this->getChildHtml('align_center_button');
        $html.= $this->getChildHtml('align_right_button');
        return $html;
    }

    protected function _prepareCollection()
    {
        $featureId = Mage::helper('folk_content')->getCurrentFeatureId();
        $collection = Mage::helper('folk_content')->getFeatureItemCollection($featureId);
        $collection->addFieldToFilter('overlay_type', array('in' => array('text', 'image')));

        $collection->getSelect()
            ->joinLeft(
                array('item_image' => $collection->getTable('folk_content/itemimage')),
                'main_table.item_id = item_image.item_id',
                array(
                    'image_width'   => 'item_image.width',
                    'image_url'     => 'item_image.link_url',
                    'image_src'     => 'item_image.image'
                )
            )
            ->joinLeft(
                array('item_text' => $collection->getTable('folk_content/itemtext')),
                'main_table.item_id = item_text.item_id',
                array(
                    'text_width'    => 'item_text.width',
                    'text_url'      => 'item_text.link_url',
                    'text_content'  => 'item_text.text',
                    'item_text.font_size',
                    'item_text.font_face',
                    'item_text.colour',
                    'item_text.alignment',
                    'item_text.line_height',
                    'item_text.letter_spacing',
                    'item_text.bold',
                    'item_text.underlined',
                    'item_text.italic'
                )
            );
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        // List out the IDs
        $this->addColumn('item_id', array(
            'header'            => $this->__('ID'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
            'sortable'          => true,
            'width'             => '60px',
            'index'             => 'item_id',
            'column_css_class'  => 'item_id',
            'name'              => 'item_id',
            'field_name'        => 'item_id',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('feature_id', array(
            'header'            => $this->__('Feature ID'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'feature_id',
            'name'              => 'feature_id',
            'field_name'        => 'feature_id',
            'field_name_group'  => 'item_overlay'
        ));

        // List out the GENERIC DATA
        $this->addColumn('label', array(
            'header'            => $this->__('Label'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
            'sortable'          => true,
            'index'             => 'label',
            'name'              => 'label',
            'field_name'        => 'label',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('overlay_type', array(
            'header'            => $this->__('Overlay Type'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_label',
            'index'             => 'overlay_type',
            'type'              => 'options',
            'options'           => array(
                'text'              => 'Text',
                'image'             => 'Image'
            ),
            'column_css_class'  => 'overlay_type',
            'name'              => 'overlay_type',
            'field_name'        => 'overlay_type',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('coord_x', array(
            'header'            => $this->__('X Coordinate'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'coord_x',
            'name'              => 'coord_x',
            'field_name'        => 'coord_x',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('coord_y', array(
            'header'            => $this->__('Y Coordinate'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'coord_y',
            'name'              => 'coord_y',
            'field_name'        => 'coord_y',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('z_index', array(
            'header'            => $this->__('Z Index (Depth)'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_zindextextfield',
            'filter'            => false,
            'sortable'          => true,
            'width'             => '50px',
            'index'             => 'z_index',
            'name'              => 'z_index',
            'field_name'        => 'z_index',
            'field_name_group'  => 'item_overlay'
        ));

        // List out the IMAGE DATA
        $this->addColumn('image_width', array(
            'header'            => $this->__('Image Width'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'image_width',
            'name'              => 'image_width',
            'field_name'        => 'image_width',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('image_url', array(
            'header'            => $this->__('Image Url'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'image_url',
            'name'              => 'image_url',
            'field_name'        => 'image_url',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('image_src', array(
            'header'            => $this->__('Image'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'image_src',
            'name'              => 'image_src',
            'field_name'        => 'image_src',
            'field_name_group'  => 'item_overlay'
        ));

        // List out the TEXT DATA
        $this->addColumn('text_width', array(
            'header'            => $this->__('Text Width'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_textfield',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'text_width',
            'name'              => 'text_width',
            'field_name'        => 'text_width',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('text_url', array(
            'header'            => $this->__('Text Url'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'text_url',
            'name'              => 'text_url',
            'field_name'        => 'text_url',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('text_content', array(
            'header'            => $this->__('Text'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'text_content',
            'name'              => 'text_content',
            'field_name'        => 'text_content',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('font_size', array(
            'header'            => $this->__('Font Size'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'font_size',
            'name'              => 'font_size',
            'field_name'        => 'font_size',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('font_face', array(
            'header'            => $this->__('Font Face'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'font_face',
            'name'              => 'font_face',
            'field_name'        => 'font_face',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('colour', array(
            'header'            => $this->__('Colour'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'colour',
            'name'              => 'colour',
            'field_name'        => 'colour',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('alignment', array(
            'header'            => $this->__('Alignment'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'alignment',
            'name'              => 'alignment',
            'field_name'        => 'alignment',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('line_height', array(
            'header'            => $this->__('Line Height'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'line_height',
            'name'              => 'line_height',
            'field_name'        => 'line_height',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('letter_spacing', array(
            'header'            => $this->__('Letter Spacing'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'letter_spacing',
            'name'              => 'letter_spacing',
            'field_name'        => 'letter_spacing',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('bold', array(
            'header'            => $this->__('Bold'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'bold',
            'type'              => 'options',
            'options'           => array(
                0                   => 'No',
                1                   => 'Yes'
            ),
            'name'              => 'bold',
            'field_name'        => 'bold',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('underlined', array(
            'header'            => $this->__('Underlined'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'underlined',
            'type'              => 'options',
            'options'           => array(
                0                   => 'No',
                1                   => 'Yes'
            ),
            'name'              => 'underlined',
            'field_name'        => 'underlined',
            'field_name_group'  => 'item_overlay'
        ));
        $this->addColumn('italic', array(
            'header'            => $this->__('Italic'),
            'renderer'          => 'folk_content/adminhtml_widget_grid_column_renderer_hidden',
            'column_css_class'  => 'no-display',
            'header_css_class'  => 'no-display',
            'index'             => 'italic',
            'type'              => 'options',
            'options'           => array(
                0                   => 'No',
                1                   => 'Yes'
            ),
            'name'              => 'italic',
            'field_name'        => 'italic',
            'field_name_group'  => 'item_overlay'
        ));

        $this->addColumn('action', array(
            'header' => $this->__('Action'),
            'width' => '100px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => $this->__('Edit'),
                    'url' => array('base' => '*/item/edit'),
                    'field' => 'id',
                )
            ),
            'filter' => false,
            'sortable' => false,
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/itemgrid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        $feature_id = '';
        if (Mage::registry('current_feature')) {
            $feature_id = Mage::registry('current_feature')->getId();
        }

        //return $this->getUrl('*/item/edit'.$row->getOverlayType().'', array('feature' => $feature_id,'id' => $row->getId()));
        return null;
    }
}
