<?php

class Folk_Content_Block_Adminhtml_Feature_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'folk_content';
        $this->_controller = 'adminhtml_feature';
        $this->_mode = 'edit';

        $this->_updateButton('save', 'label', $this->__('Save Feature'));
        $this->_updateButton('delete', 'label', $this->__('Delete Feature'));
    }



    protected function _prepareLayout()
    {
        $this->_removeButton('reset');

        $this->_updateButton('save', 'label', $this->__('Save Feature'));
        $this->_updateButton('delete', 'label', $this->__('Delete Feature'));

        if (Mage::helper('folk_content')->getReturnUrl()) {
            $this->_updateButton('back', 'onclick',"setLocation('" . $this->getUrl(Mage::helper('folk_content')->getReturnUrl()) ."')" );
        } else {
            $this->_updateButton('back', 'onclick',"setLocation('" . $this->getUrl('*/*/index/') ."')" );
        }

        // If making a new feature it has to return back to the layout so it can add it
        if (Mage::registry('current_feature')->getId()) {
            $this->_addButton('save_and_continue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
            ), -100);
        }

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'continue/edit/');
            }";
        return parent::_prepareLayout();
    }


    public function getHeaderText()
    {
        if (Mage::registry('current_feature')->getId()) {
            return $this->__('Edit Feature');
        }
        else {
            return $this->__('New Feature');
        }
    }

}