<?php

class Folk_Content_Block_Adminhtml_Feature_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('switch_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Feature management'));
    }

    /**
     * Add tabs content
     *
     * @return Folk_Content_Block_Adminhtml_Feature_Edit_Tab
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label'   => $this->__('Edit Feature Details'),
            'title'   => $this->__('Edit Feature Details'),
            'content' => $this->getLayout()
                    ->createBlock('folk_content/adminhtml_feature_edit_tab_general')
                    ->toHtml(),
        ));

        if (Mage::registry('current_feature')->getBgImage() != '') {
            $this->addTab('item', array(
                'label'   => $this->__('Edit Overlay Items'),
                'title'   => $this->__('Edit Overlay Items'),
                'content' => $this->getLayout()
                    ->createBlock('folk_content/adminhtml_feature_edit_tab_item')
                    ->toHtml(),
            ));

            $this->addTab('product', array(
                'label'   => $this->__('Edit Overlay Products'),
                'title'   => $this->__('Edit Overlay Products'),
                'content' => $this->getLayout()
                    ->createBlock('folk_content/adminhtml_feature_edit_tab_product')
                    ->toHtml(),
            ));
        }

        return parent::_beforeToHtml();
    }
}