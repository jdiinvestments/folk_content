<?php

class Folk_Content_Block_Adminhtml_Item_Editimage extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'folk_content';
        $this->_controller = 'adminhtml_item';
        $this->_mode = 'editimage';
    }



    protected function _prepareLayout()
    {
        $parentFeature = $this->getRequest()->getParam('feature');
        $this->_updateButton('back', 'onclick',"setLocation('" . $this->getUrl('*/feature/edit/id/' . $parentFeature) . "')");

        $this->_updateButton('save', 'label', $this->__('Save Image'));
        $this->_updateButton('delete', 'label', $this->__('Delete Image'));

        return parent::_prepareLayout();
    }


    public function getHeaderText()
    {
        if (Mage::registry('current_item')->getId()) {
            return $this->__('Edit Image');
        } else {
            return $this->__('New Image');
        }
    }

}