<?php

class Folk_Content_Block_Adminhtml_Item_Edittext extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'folk_content';
        $this->_controller = 'adminhtml_item';
        $this->_mode = 'edittext';
    }



    protected function _prepareLayout()
    {
        $parentFeature = $this->getRequest()->getParam('feature');
        $this->_updateButton('back', 'onclick',"setLocation('" . $this->getUrl('*/feature/edit/id/' . $parentFeature) . "')");

        $this->_updateButton('save', 'label', $this->__('Save Text'));
        $this->_updateButton('delete', 'label', $this->__('Delete Text'));

        return parent::_prepareLayout();
    }


    public function getHeaderText()
    {
        if (Mage::registry('current_item')->getId()) {
            return $this->__('Edit Text');
        } else {
            return $this->__('New Text');
        }
    }

}