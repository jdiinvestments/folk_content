<?php

class Folk_Content_Block_Adminhtml_Item_Editimage_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Add the form fields
     *
     * @return Folk_Home_Block_Adminhtml_Features_Edit_Tab_General
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('itemimage_');

        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $this->__('Image Overlay')
        ));

        $fieldset->addField('item_id', 'hidden', array(
            'label' => $this->__('item_id'),
            'name' => 'item_id',
        ));

        $fieldset->addField('feature_id', 'hidden', array(
            'label' => $this->__('feature_id'),
            'name' => 'feature_id',
        ));

        $fieldset->addField('coord_x', 'hidden', array(
            'label' => $this->__('coord_x'),
            'name' => 'coord_x',
        ));

        $fieldset->addField('coord_y', 'hidden', array(
            'label' => $this->__('coord_y'),
            'name' => 'coord_y',
        ));

        $fieldset->addField('overlay_type', 'hidden', array(
            'label' => 'overlay_type',
            'name' => 'overlay_type',
        ));

        $fieldset->addField('label', 'text', array(
            'label' => $this->__('label'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'label',
            'after_element_html' => 'Note: For admin use only'
        ));

        $fieldset->addField('item_image_id', 'hidden', array(
            'label' => $this->__('item_image_id'),
            'name' => 'item_image_id',
        ));

        $fieldset->addField('width', 'hidden', array(
            'label' => $this->__('width'),
            'name' => 'width',
        ));

        $fieldset->addField('link_url', 'text', array(
            'label' => $this->__('link_url'),
            'name' => 'link_url',
        ));

        $fieldset->addField('image', 'image', array(
            'label' => $this->__('Image'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'item[image]',
        ));


        $form->addValues($this->_getFormData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     *
     * @return array
     */
    protected function _getFormData()
    {
        $data = Mage::getSingleton('adminhtml/session')->getFormData();

        if (Mage::registry('current_item')->getData()) {
            $data = Mage::registry('current_item')->getData();
        }
        if (Mage::registry('current_item_image')->getData()) {
            $data = array_merge($data, Mage::registry('current_item_image')->getData());
        }

        return (array)$data;
    }
}