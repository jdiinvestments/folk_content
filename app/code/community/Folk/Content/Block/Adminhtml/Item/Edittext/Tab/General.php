<?php

class Folk_Content_Block_Adminhtml_Item_Edittext_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Add the form fields
     *
     * @return Folk_Home_Block_Adminhtml_Features_Edit_Tab_General
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('itemtext_');

        $fieldset = $form->addFieldset('general_form', array(
            'legend'    => $this->__('Text Overlay')
        ));

        $fieldset->addField('item_id', 'hidden', array(
            'label'     => $this->__('item_id'),
            'name'      => 'item_id',
        ));

        $fieldset->addField('feature_id', 'hidden', array(
            'label'     => $this->__('feature_id'),
            'name'      => 'feature_id',
        ));

        $fieldset->addField('coord_x', 'hidden', array(
            'label'     => $this->__('coord_x'),
            'name'      => 'coord_x',
        ));

        $fieldset->addField('coord_y', 'hidden', array(
            'label'     => $this->__('coord_y'),
            'name'      => 'coord_y',
        ));

        $fieldset->addField('overlay_type', 'hidden', array(
            'label'     => 'overlay_type',
            'name'      => 'overlay_type',
        ));

        $fieldset->addField('label', 'text', array(
            'label'     => $this->__('label'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'label',
            'after_element_html' => 'Note: For admin use only'
        ));

        $fieldset->addField('item_text_id', 'hidden', array(
            'label'     => $this->__('item_text_id'),
            'name'      => 'item_text_id',
        ));

        $fieldset->addField('width', 'hidden', array(
            'label'     => $this->__('width'),
            'name'      => 'width',
        ));

        $fieldset->addField('link_url', 'text', array(
            'label'     => $this->__('link_url'),
            'name'      => 'link_url',
        ));

        $fieldset->addField('text', 'textarea', array(
            'label'     => $this->__('text'),
            'name'      => 'text',
            'class'     => 'required-entry',
            'required'  => true,
        ));


        $fieldset->addField('font_size', 'select', array(
            'label'     => $this->__('font_size'),
            'name'      => 'font_size',
            'value'     => 1,
            'after_element_html' => 'Note: Enter the font-size to the desired px value, but the font-size will resize as the feature expands or shrinks. The px value will only be used when the width of the feature is exactly 1000px wide.',
            'values'    => array(
                array('value' => 0.5,   'label' => '0.5em (≈8px)'),     // 8px
                array('value' => 0.563, 'label' => '0.563em (≈9px)'),   // 9px
                array('value' => 0.625, 'label' => '0.625em (≈10px)'),  // 10px
                array('value' => 0.688, 'label' => '0.688em (≈11px)'),  // 11px
                array('value' => 0.75,  'label' => '0.75em (≈12px)'),   // 12px
                array('value' => 0.813, 'label' => '0.813em (≈13px)'),  // 13px
                array('value' => 0.875, 'label' => '0.875em (≈14px)'),  // 14px
                array('value' => 0.938, 'label' => '0.938em (≈15px)'),  // 15px
                array('value' => 1,     'label' => '1em (≈16px)'),      // 16px
                array('value' => 1.063, 'label' => '1.063em (≈17px)'),  // 17px
                array('value' => 1.125, 'label' => '1.125em (≈18px)'),  // 18px
                array('value' => 1.188, 'label' => '1.188em (≈19px)'),  // 19px
                array('value' => 1.25,  'label' => '1.25em (≈20px)'),   // 20px
                array('value' => 1.313, 'label' => '1.313em (≈21px)'),  // 21px
                array('value' => 1.375, 'label' => '1.375em (≈22px)'),  // 22px
                array('value' => 1.438, 'label' => '1.438em (≈23px)'),  // 23px
                array('value' => 1.5,   'label' => '1.5em (≈24px)'),    // 24px
                array('value' => 1.563, 'label' => '1.563em (≈25px)'),  // 25px
                array('value' => 1.625, 'label' => '1.625em (≈26px)'),  // 26px
                array('value' => 1.688, 'label' => '1.688em (≈27px)'),  // 27px
                array('value' => 1.750, 'label' => '1.75em (≈28px)'),   // 28px
                array('value' => 1.813, 'label' => '1.813em (≈29px)'),  // 29px
                array('value' => 1.875, 'label' => '1.875em (≈30px)'),  // 30px
                array('value' => 1.938, 'label' => '1.938em (≈31px)'),  // 31px
                array('value' => 2,     'label' => '2em (≈32px)'),      // 32px
                array('value' => 2.0625,'label' => '2.0625em (≈33px)'), // 33px
                array('value' => 2.125, 'label' => '2.125em (≈34px)'),  // 34px
                array('value' => 2.1875,'label' => '2.1875em (≈35px)'), // 35px
                array('value' => 2.25,  'label' => '2.25em (≈36px)'),   // 36px
                array('value' => 2.3125,'label' => '2.3125em (≈37px)'), // 37px
                array('value' => 2.375, 'label' => '2.375em (≈38px)'),  // 38px
                array('value' => 2.4375,'label' => '2.4375em (≈39px)'), // 39px
                array('value' => 2.5,   'label' => '2.5em (≈40px)'),    // 40px
                array('value' => 2.5625,'label' => '2.5625em (≈41px)'), // 41px
                array('value' => 2.625, 'label' => '2.625em (≈42px)'),  // 42px
                array('value' => 2.6875,'label' => '2.6875em (≈43px)'), // 43px
                array('value' => 2.75,  'label' => '2.75em (≈44px)'),   // 44px
                array('value' => 2.8125,'label' => '2.8125em (≈45px)'), // 45px
                array('value' => 2.875, 'label' => '2.875em (≈46px)'),  // 46px
                array('value' => 2.9375,'label' => '2.9375em (≈47px)'), // 47px
                array('value' => 3,     'label' => '3em (≈48px)'),      // 48px
                array('value' => 3.0625,'label' => '3.0625em (≈49px)'), // 49px
                array('value' => 3.125, 'label' => '3.125em (≈50px)'),  // 50px
                array('value' => 3.1875,'label' => '3.1875em (≈51px)'), // 51px
                array('value' => 3.25,  'label' => '3.25em (≈52px)'),   // 52px
                array('value' => 3.3125,'label' => '3.3125em (≈53px)'), // 53px
                array('value' => 3.375, 'label' => '3.375em (≈54px)'),  // 54px
                array('value' => 3.4375,'label' => '3.4375em (≈55px)'), // 55px
                array('value' => 3.5,   'label' => '3.5em (≈56px)'),    // 56px
                array('value' => 3.5625,'label' => '3.5625em (≈57px)'), // 57px
                array('value' => 3.625, 'label' => '3.625em (≈58px)'),  // 58px
                array('value' => 3.6875,'label' => '3.6875em (≈59px)'), // 59px
                array('value' => 3.75,  'label' => '3.75em (≈60px)'),   // 60px
                array('value' => 3.8125,'label' => '3.8125em (≈61px)'), // 61px
                array('value' => 3.875, 'label' => '3.875em (≈62px)'),  // 62px
                array('value' => 3.9375,'label' => '3.9375em (≈63px)'), // 63px
                array('value' => 4,     'label' => '4em (≈64px)')       // 64px
            )
        ));

        $fieldset->addField('font_face', 'select', array(
            'label'     => $this->__('font_face'),
            'name'      => 'font_face',
            'values'    => array(
                array('value' => 'CAL Aperto W01', 'label' => 'Aperto'),
                array('value' => 'Didot W01 Italic', 'label' => 'Didot'),
                array('value' => 'Cardo', 'label' => 'Cardo'),
                array('value' => 'Avenir W01', 'label' => 'Avenir W01'),
                array('value' => 'Proxima N W01 Light', 'label' => 'Proxima Nova Light'),
                array('value' => 'Proxima N W01 Reg', 'label' => 'Proxima Nova Regular'),
                array('value' => 'Proxima N W01 Bold', 'label' => 'Proxima Nova Bold'),
                array('value' => 'Carolyna W00 Black', 'label' => 'Carolyna Black'),
                array('value' => 'Georgia, serif', 'label' => 'Georgia'),
                array('value' => '&quot;Palatino Linotype&quot;, &quot;Book Antiqua&quot;, Palatino, serif', 'label' => 'Palatino Linotype/Book Antiqua'),
                array('value' => '&quot;Times New Roman&quot;, Times, serif', 'label' => 'Times New Roman'),
                array('value' => 'Arial, Helvetica, sans-serif', 'label' => 'Arial/Helvetica'),
                array('value' => '&quot;Comic Sans MS&quot;, cursive, sans-serif', 'label' => 'Comic Sans MS'),
                array('value' => 'Impact, Charcoal, sans-serif', 'label' => 'Impact'),
                array('value' => '&quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif', 'label' => 'Lucida Sans Unicode'),
                array('value' => 'Tahoma, Geneva, sans-serif', 'label' => 'Tahoma'),
                array('value' => '&quot;Trebuchet MS&quot;, Helvetica, sans-serif', 'label' => 'Trebuchet MS'),
                array('value' => 'Verdana, Geneva, sans-serif', 'label' => 'Verdana'),
                array('value' => '&quot;Courier New&quot;, Courier, monospace', 'label' => 'Courier New'),
                array('value' => '&quot;Lucida Console&quot;, Monaco, monospace', 'label' => 'Lucida Console'),
                array('value' => '&quot;Gill Sans W01 Light&quot;,Helvetica,Roboto,Arial,sans-serif', 'label' => 'Gill Sans W01 Light'),
                array('value' => '&quot;Dorothea W01 Regular&quot;,Helvetica,Roboto,Arial,sans-serif', 'label' => 'Dorothea W01 Regular'),
            )
        ));


        $fieldset->addField('colour', 'text', array(
            'label'     => $this->__('Colour'),
            'name'      => 'colour',
            'class'     => 'color {required:false, adjust:false, hash:true}',
            'value'     => '#000000'
        ));

        $fieldset->addField('alignment', 'select', array(
            'label'     => $this->__('alignment'),
            'name'      => 'alignment',
            'values'    => array(
                array('value' => 'left', 'label' => 'Left'),
                array('value' => 'center', 'label' => 'Center'),
                array('value' => 'right', 'label' => 'Right')
            )
        ));

        $fieldset->addField('line_height', 'text', array(
            'label'     => $this->__('line_height'),
            'name'      => 'line_height',
            'value'     => 1,
            'after_element_html' => 'Line height should be in EMs and relate to the font size or enter "auto"',
        ));

        $fieldset->addField('letter_spacing', 'text', array(
            'label'     => $this->__('letter_spacing'),
            'name'      => 'letter_spacing',
            'value'     => 'normal',
            'after_element_html' => 'Letter spacing should be in px or enter "normal"',
        ));

        $fieldset->addField('bold', 'select', array(
            'label'     => $this->__('bold'),
            'name'      => 'bold',
            'default'   => 0,
            'values'    => array(
                array('value' => 0, 'label' => 'No'),
                array('value' => 1, 'label' => 'Yes')
            )
        ));

        $fieldset->addField('underlined', 'select', array(
            'label'     => $this->__('underlined'),
            'name'      => 'underlined',
            'default'   => 0,
            'values'    => array(
                array('value' => 0, 'label' => 'No'),
                array('value' => 1, 'label' => 'Yes')
            )
        ));

        $fieldset->addField('italic', 'select', array(
            'label'     => $this->__('italic'),
            'name'      => 'italic',
            'default'   => 0,
            'values'    => array(
                array('value' => 0, 'label' => 'No'),
                array('value' => 1, 'label' => 'Yes')
            )
        ));


        $form->addValues($this->_getFormData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     *
     * @return array
     */
    protected function _getFormData()
    {
        $data = Mage::getSingleton('adminhtml/session')->getFormData();

        if (Mage::registry('current_item')->getData()) {
            $data = Mage::registry('current_item')->getData();
        }
        if (Mage::registry('current_item_text')->getData()) {
            $data = array_merge($data, Mage::registry('current_item_text')->getData());
        }
        return (array)$data;
    }
}