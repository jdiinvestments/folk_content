<?php

class Folk_Content_Block_Adminhtml_Item_Edittext_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('switch_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Text management'));
    }

    /**
     * Add tabs content
     *
     * @return Folk_Content_Block_Adminhtml_Feature_Edit_Tab
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label'   => $this->__('Edit Text Details'),
            'title'   => $this->__('Edit Text Details'),
            'content' => $this->getLayout()
                ->createBlock('folk_content/adminhtml_item_edittext_tab_general')
                ->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}