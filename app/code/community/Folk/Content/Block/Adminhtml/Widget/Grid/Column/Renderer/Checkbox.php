<?php
class Folk_Content_Block_Adminhtml_Widget_Grid_Column_Renderer_Checkbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_defaultWidth = 55;
    protected $_values;

    /**
     * Returns values of the column
     *
     * @return array
     */
    public function getValues()
    {
        if (is_null($this->_values)) {
            $this->_values = $this->getColumn()->getData('values') ? $this->getColumn()->getData('values') : array();
        }
        return $this->_values;
    }

    public function render(Varien_Object $row)
    {
        $values = $this->getColumn()->getValues();

        $value = $row->getData($this->getColumn()->getIndex());
        if (is_array($values)) {
            $checked = in_array($value, $values) ? ' checked="checked"' : '';
        } else {
            $checked = ($value === $this->getColumn()->getValue()) ? ' checked="checked"' : '';
        }

        if ($this->getNoObjectId() || $this->getColumn()->getUseIndex()) {
            $v = $value;
        } else {
            $v = ($row->getId() != "") ? $row->getId() : $value;
        }

        return $this->_getCheckboxHtml($v, $checked, $row);
    }

    protected function _getCheckboxHtml($value, $checked, $row)
    {
        $html = '<input type="checkbox" ';
        if ($this->getColumn()->getFieldNameGroup()) {
            $html .= 'name="' . $this->getColumn()->getFieldNameGroup() . '[' . $row->getId() . '][' . $this->getColumn()->getFieldName() . ']" ';
        } else {
            $html .= 'name="' . $this->getColumn()->getFieldName() . '" ';
        }
        $html .= 'value="' . $this->escapeHtml($value) . '" ';
        $html .= 'class="' . ($this->getColumn()->getInlineCss() ? $this->getColumn()->getInlineCss() : 'checkbox') . '"';
        $html .= $checked . $this->getDisabled() . '/>';
        return $html;
    }

    /**
     * Renders header of the column
     *
     * @return string
     */
    public function renderHeader()
    {
        if ($this->getColumn()->getHeader()) {
            return parent::renderHeader();
        }

        $checked = '';
        if ($filter = $this->getColumn()->getFilter()) {
            $checked = $filter->getValue() ? ' checked="checked"' : '';
        }

        $disabled = '';
        if ($this->getColumn()->getDisabled()) {
            $disabled = ' disabled="disabled"';
        }
        $html = '<input type="checkbox" ';
        $html .= 'name="' . $this->getColumn()->getFieldName() . '" ';
        $html .= 'onclick="' . $this->getColumn()->getGrid()->getJsObjectName() . '.checkCheckboxes(this)" ';
        $html .= 'class="checkbox"' . $checked . $disabled . ' ';
        $html .= 'title="' . Mage::helper('adminhtml')->__('Select All') . '"/>';
        return $html;
    }

}
