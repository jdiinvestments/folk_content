<?php

class Folk_Content_Block_Adminhtml_Widget_Grid_Column_Renderer_Textfield extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_values;

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $rowValue = $row->getData($this->getColumn()->getIndex());
        $html = '<input type="text" ';
        $html .= 'name="'.$this->getColumn()->getFieldNameGroup().'[' . $row->getId() . '][' . $this->getColumn()->getId() . ']" ';
        $html .= 'value="' . str_replace('"','&quot;',$rowValue) . '"';
        $html .= 'class="input-text ' . $this->getColumn()->getInlineCss() . '"/>';
        return $html;
    }

}
