<?php

class Folk_Content_Block_Adminhtml_Widget_Grid_Column_Renderer_Zindextextfield extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected $_values;

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $rowValue = $row->getData($this->getColumn()->getIndex());
        $html =  '<button id="zindex_minus_button_'.$row->getId().'" ';
        $html .= 'title="Add Text Overlay" type="button" ';
        $html .= 'class="scalable task zindex_minus_button" ';
        $html .= 'style="height:20px;width:23px;vertical-align:bottom;"';
        $html .= 'value='.$row->getId().'><span><span><span>-</span></span></span></button>';

        $html .= '<input type="text" ';
        $html .= 'name="'.$this->getColumn()->getFieldNameGroup().'[' . $row->getId() . '][' . $this->getColumn()->getId() . ']" ';
        $html .= 'value="' . str_replace('"','&quot;',$rowValue) . '"';
        $html .= 'id="zindex_field_'.$row->getId().'"';
        $html .= 'style="width:50px;text-align:center;"';
        $html .= 'class="input-text zindex_field' . $this->getColumn()->getInlineCss() . '"/>';

        $html .= '<button id="zindex_plus_button_'.$row->getId().'" ';
        $html .= 'title="Add Text Overlay" ';
        $html .= 'type="button" ';
        $html .= 'class="scalable task zindex_plus_button" ';
        $html .= 'style="height:20px;width:23px;vertical-align:bottom;"';
        $html .= 'value="'.$row->getId().'"><span><span><span>+</span></span></span></button>';
        return $html;
    }

}
