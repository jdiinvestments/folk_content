<?php

class Folk_Content_Block_Adminhtml_Feature extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Initialize grid container settings
     *
     * The grid block class must be:
     *
     * $this->_blockGroup . '/' . $this->_controller . '_grid'
     * i.e. folk_warehouse/adminhtml_warehouse_grid
     */
    protected function _construct()
    {
        $this->_blockGroup = 'folk_content';
        $this->_controller = 'adminhtml_feature';
        $this->_headerText = $this->__('Manage Features');
        $this->_addButtonLabel = $this->__('Add Feature');

        parent::_construct();
	
    }
}