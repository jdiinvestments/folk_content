<?php

class Folk_Content_Block_Feature extends Mage_Core_Block_Template
{

    /**
     * Initialize grid container settings
     *
     * The grid block class must be:
     *
     * $this->_blockGroup . '/' . $this->_controller . '_grid'
     * i.e. folk_warehouse/adminhtml_warehouse_grid
     */
    protected function _construct()
    {
        parent::_construct();
    }


    public function getFeature($_id)
    {
        $model = Mage::getModel('folk_content/feature');
        if ($_id > 0) {
            try {
                $model->load($_id);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        return $model;
    }

    public function getFeatureItems($_id)
    {
        $collection = Mage::helper('folk_content')->getFeatureItemCollection($_id);
        $collection->addFieldToFilter('overlay_type', array('in' => array('text', 'image')));

        $collection->getSelect()
            ->joinLeft(
                array('item_image' => $collection->getTable('folk_content/itemimage')),
                'main_table.item_id = item_image.item_id',
                array(
                    'image_width'   => 'item_image.width',
                    'image_url'     => 'item_image.link_url',
                    'image_src'     => 'item_image.image'
                )
            )
            ->joinLeft(
                array('item_text' => $collection->getTable('folk_content/itemtext')),
                'main_table.item_id = item_text.item_id',
                array(
                    'text_width'    => 'item_text.width',
                    'text_url'      => 'item_text.link_url',
                    'text_content'  => 'item_text.text',
                    'item_text.font_size',
                    'item_text.font_face',
                    'item_text.colour',
                    'item_text.alignment',
                    'item_text.line_height',
                    'item_text.letter_spacing',
                    'item_text.bold',
                    'item_text.underlined',
                    'item_text.italic'
                )
            );
        $this->setCollection($collection);
        return $collection;
    }

}