<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tilley
 * Date: 17/03/2014
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */
class Folk_Content_Helper_Data extends Mage_Core_Helper_Abstract {

    // Get the current registered feature
    public function getCurrentFeatureId() {
        return Mage::registry('current_feature')->getId();
    }

    // Gets a collection of items for a feature
    public function getFeature($featureId) {
        return Mage::getModel('folk_content/feature')->load($featureId);
    }

    // Gets a collection of items for a feature
    public function getFeatureItemCollection($featureId) {
        return Mage::getModel('folk_content/item')->getCollection()->addFieldToFilter('feature_id',array('eq'=>$featureId));
    }

    /* Allows external modules to grab a collection of features using a comma delimited string or array */
    public function getFeatureCollection($featureList) {
        // Convert to array if it isn't already
        if (!is_array($featureList)) {
            $featureList = explode(',', $featureList);
        }

        $collection = null;
        if (count($featureList)) {
            $collection = Mage::getModel('folk_content/feature')
                ->getCollection()
                ->addFieldToFilter('feature_id', array('in' => $featureList));
        }
        return $collection;
    }

    // Returns the server directory for the folkcontent images
    public function getImagePath()
    {
        return Mage::getBaseDir('media') . DS . 'folkcontent' . DS;
    }

    // Returns the full url for an image
    public function getImageUrl($img)
    {
        if (!$img) {
            return '';
        }
        return Mage::getBaseUrl('media') . $img;
    }

    // Checks if a url is absolute or relative
    public function getUrl($url) {
        if (!preg_match("@^https?://@", $url)) {
            $url = Mage::getBaseUrl().$url;
        }
        return $url;
    }

    // Used in the slider template to make sure all the ids exist
    public function checkFeatureArray($feature_array) {
        $sanitised_array = array();
        if (is_array($feature_array)) {
            for ($i = 0; $i < count($feature_array); $i++) {
                if ($this->getFeature($feature_array[$i])->getId() && $this->getFeature($feature_array[$i])->getEnabled()) {
                    array_push($sanitised_array, $feature_array[$i]);
                }
            }
        }
        return $sanitised_array;
    }

    /*
     * These Functions allow you to call the output of the features. The first one calls a single feature. The
     * second calls out a whole slider.
     */

    // Allows you to output the html for a single feature to anywhere in a magento application
    public function getFeatureHtml($featureId) {
        $featureBlock = Mage::app()->getLayout()->createBlock("folk_content/feature");
        $featureBlock ->setData("feature_id", $featureId);
        $featureBlock->setTemplate("folk/content/feature.phtml");
        return $featureBlock->toHtml();
    }

    // Allows you to output a slider of multiple features
    public function getSliderHtml($feature_array) {
        if (!is_array($feature_array) && is_string($feature_array)) {
            $feature_array = explode(',', $feature_array);
        }

        $html = false;
        if (is_array($feature_array) && count($feature_array)) {
            $featureBlock = Mage::app()->getLayout()->createBlock("folk_content/feature");
            $featureBlock ->setData("feature_array", $feature_array);
            $featureBlock->setTemplate("folk/content/slider.phtml");
            $html = $featureBlock->toHtml();
        }
        return $html;
    }

    /*
     * These functions deal with the external extensions. They are used to pass data between the extension and the
     * Folk_Content module. An extension should set a return url when loading a list of features (or create feature
     * action). When the Folk_Content module finishes an action will pass back an action and a feature id. Actions
     * are [back] [save] and [delete]. The extension should have a single entry point for the return url which will
     * have different actions depending on the return value. A user should be able to navigate around the Folk_Content
     * module without reinitiating the return url. The extension should use the clear function to ensure unused data
     * doesn't confuse the Folk_Content module.
     */

    // Sets the return url for creating or updating features
    public function setReturnUrl($returnUrl) {
        Mage::getSingleton('core/session')->setFolkContentReturnUrl($returnUrl);
    }

    // Returns the return url for creating or updating features
    public function getReturnUrl() {
        return Mage::getSingleton('core/session')->getFolkContentReturnUrl();
    }

    // Sets the return action for creating or updating features
    public function setReturnAction($returnAction) {
        Mage::getSingleton('core/session')->setFolkContentReturnAction($returnAction);
    }

    // Returns the return action for creating or updating features
    public function getReturnAction() {
        return Mage::getSingleton('core/session')->getFolkContentReturnAction();
    }

    // Sets the return action for creating or updating features
    public function setReturnFeatureId($featureId) {
        Mage::getSingleton('core/session')->setFolkContentReturnFeatureId($featureId);
    }

    // Returns the return action for creating or updating features
    public function getReturnFeatureId() {
        return Mage::getSingleton('core/session')->getFolkContentReturnFeatureId();
    }

    // Clears the session data for the return url. Extending modules should use this when they receive a callback
    public function clearReturnData() {
        Mage::getSingleton('core/session')->unsetData('folk_content_return_url');
        Mage::getSingleton('core/session')->unsetData('folk_content_return_action');
        Mage::getSingleton('core/session')->unsetData('folk_content_return_feature_id');
    }

    // Resize image method for use of non product images on site
    public function resizeImg($fileName, $width, $height = '')
    {
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $folderURL . $fileName;

        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "resized" . DS . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(FALSE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS . $fileName;
        } else {
            $resizedURL = $imageURL;
        }
        return $resizedURL;
    }

}