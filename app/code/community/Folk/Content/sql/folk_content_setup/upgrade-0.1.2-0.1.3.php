<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('folk_content/item'),
        'z_index',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => true,
            'default' => 1,
            'comment' => 'Z-Index'
        )
    );

$installer->endSetup();
