<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tilley
 * Date: 17/03/2014
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/* ************************** */
/* Install the features table */
/* ************************** */
$table = $installer->getTable('folk_content/feature');
if ($installer->tableExists($table)) {
    $installer->getConnection()->dropTable($table);
}
/** @var $ddlTable Varien_Db_Ddl_Table */
$ddlTable = $installer->getConnection()->newTable($table);
$ddlTable->addColumn('feature_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'primary'  => true,
    'identity' => true,
    'unsigned' => true,
    'nullable' => false,
), 'Entity ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => false,
    ), 'Title')
    ->addColumn('link_url', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Link Url')
    ->addColumn('bg_image', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Background Image')
    ->addColumn('mobile_image', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Mobile Image')
    ->addColumn('embed_script', Varien_Db_Ddl_Table::TYPE_TEXT, 2048, array(
        'nullable' => true,
        'default'  => ''
    ), 'Embed Script')
    ->addColumn('enabled', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable' => false,
    ), 'Enabled')
    ->setComment('Folk Content Feature');
$installer->getConnection()->createTable($ddlTable);

/* *********************** */
/* Install the items table */
/* *********************** */
$table = $installer->getTable('folk_content/item');
if ($installer->tableExists($table)) {
    $installer->getConnection()->dropTable($table);
}
/** @var $ddlTable Varien_Db_Ddl_Table */
$ddlTable = $installer->getConnection()->newTable($table);
$ddlTable->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'primary'  => true,
    'identity' => true,
    'unsigned' => true,
    'nullable' => false,
), 'Entity ID')
    ->addColumn('feature_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Feature ID')
    ->addColumn('coord_x', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => 0
    ), 'Coordinate X')
    ->addColumn('coord_y', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => 0
    ), 'Coordinate Y')
    ->addColumn('overlay_type', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Overlay Type')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => false,
    ), 'Label')
    ->setComment('Folk Content Item');
$installer->getConnection()->createTable($ddlTable);

/* ***************************** */
/* Install the item images table */
/* ***************************** */
$table = $installer->getTable('folk_content/itemimage');
if ($installer->tableExists($table)) {
    $installer->getConnection()->dropTable($table);
}
/** @var $ddlTable Varien_Db_Ddl_Table */
$ddlTable = $installer->getConnection()->newTable($table);
$ddlTable->addColumn('item_image_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'primary'  => true,
    'identity' => true,
    'unsigned' => true,
    'nullable' => false,
), 'Entity ID')
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Item ID')
    ->addColumn('width', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => 0
    ), 'Coordinate X')
    ->addColumn('link_url', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Link Url')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => false,
    ), 'Image')
    ->setComment('Folk Content Item Image');
$installer->getConnection()->createTable($ddlTable);

/* ***************************** */
/* Install the item images table */
/* ***************************** */
$table = $installer->getTable('folk_content/itemtext');
if ($installer->tableExists($table)) {
    $installer->getConnection()->dropTable($table);
}
/** @var $ddlTable Varien_Db_Ddl_Table */
$ddlTable = $installer->getConnection()->newTable($table);
$ddlTable->addColumn('item_text_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'primary'  => true,
    'identity' => true,
    'unsigned' => true,
    'nullable' => false,
), 'Entity ID')
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Item ID')
    ->addColumn('width', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => 0
    ), 'Coordinate X')
    ->addColumn('link_url', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Link Url')
    ->addColumn('text', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        'nullable' => true,
        'default'  => ''
    ), 'Text')
    ->addColumn('font_size', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Font Size')
    ->addColumn('font_face', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Font Face')
    ->addColumn('colour', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Colour')
    ->addColumn('alignment', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Alignment')
    ->addColumn('line_height', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Line Height (ems)')
    ->addColumn('letter_spacing', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default'  => ''
    ), 'Letter Spacing (px)')
    ->addColumn('bold', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable' => false,
    ), 'Enabled')
    ->addColumn('underlined', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable' => false,
    ), 'Enabled')
    ->addColumn('italic', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable' => false,
    ), 'Enabled')
    ->setComment('Folk Content Item Text');
$installer->getConnection()->createTable($ddlTable);

/* ******************************* */
/* Install the item products table */
/* ******************************* */
$table = $installer->getTable('folk_content/itemproduct');
if ($installer->tableExists($table)) {
    $installer->getConnection()->dropTable($table);
}
/** @var $ddlTable Varien_Db_Ddl_Table */
$ddlTable = $installer->getConnection()->newTable($table);
$ddlTable->addColumn('item_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
    'primary'  => true,
    'identity' => true,
    'unsigned' => true,
    'nullable' => false,
), 'Entity ID')
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Item ID')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Product ID')
    ->setComment('Folk Content Item Product');
$installer->getConnection()->createTable($ddlTable);


$installer->endSetup();