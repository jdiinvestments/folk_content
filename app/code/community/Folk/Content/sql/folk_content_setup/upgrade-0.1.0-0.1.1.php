<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();


$installer->run("
ALTER TABLE {$this->getTable('folk_content/item')} MODIFY `coord_x` float(20,15) NOT NULL DEFAULT '0.000000000000000';
ALTER TABLE {$this->getTable('folk_content/item')} MODIFY `coord_y` float(20,15) NOT NULL DEFAULT '0.000000000000000';
");

$installer->endSetup();
