<?php
class Folk_Content_Model_Feature extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('folk_content/feature');
    }



    public function parseHtml($text)
    {
        $parsedText = str_replace(chr(10), "", $text);
        $parsedText = str_replace(chr(13), "", $parsedText);
        return trim($parsedText);
    }

    public function getProductCollection()
    {
        $itemCollection = false;
        if ($this->getFeatureId())
        {
            $itemCollection = Mage::helper('folk_content')->getFeatureItemCollection($this->getFeatureId());
            $itemCollection->addFieldToFilter('overlay_type', array('in' => array('product')));

            $itemCollection->getSelect()
                ->joinLeft(
                    array('item_product' => $itemCollection->getTable('folk_content/itemproduct')),
                    'main_table.item_id = item_product.item_id',
                    array( // Don't include item_id
                        'item_product_id',
                        'product_id'
                    )
                );
        }
        return $itemCollection;
    }

    public function getProductCollectionArray()
    {
        $collectionArray = array();
        $collection = $this->getProductCollection();
        if ($collection->count())
        {
            foreach ($collection as $item)
            {
                $collectionArray[$item->getItemId()] = $item->getData();

                if ($product_id = $collectionArray[$item->getItemId()]['product_id'])
                {

                    $product = Mage::getModel('catalog/product')->load($product_id);

                    if ($product->getId())
                    {
                        // Get Info Window HTML

                        Mage::register('product',Mage::getModel('catalog/product')->load($product->getId()));
                        $infoWindowBlock = Mage::app()->getLayout()->createBlock("catalog/product_view")
                            ->setProduct($product)
                            ->setTemplate("folk/content/info-window.phtml");
                        $collectionArray[$item->getItemId()]['info_window_html'] = $infoWindowBlock->toHtml();
                        Mage::unregister('product');





                        $priceBlock = Mage::helper('core')->currency($product->getPrice());


                        $collectionArray[$item->getItemId()]['form_url'] = Mage::helper('checkout/cart')->getAddUrl($product);
                        // This could be customisable to use Ajax Add to Cart instead like below
                        //$collectionArray[$item->getItemId()]['form_url'] = Mage::getUrl('addtocart/ajax/add', array('product' => $product->getId()));

                        $collectionArray[$item->getItemId()]['wishlist_url'] = Mage::helper('wishlist')->getAddUrl($product); // This isn't usually used

                        $collectionArray[$item->getItemId()]['product']['name'] = $product->getName();
                        $collectionArray[$item->getItemId()]['product']['price'] = $this->parseHtml($priceBlock);
                        $collectionArray[$item->getItemId()]['product']['sku'] = $product->getSku();
                        $collectionArray[$item->getItemId()]['product']['short_description'] = ''.$product->getShortDescription().'';


                        $collectionArray[$item->getItemId()]['product']['type'] = $product->getTypeId();
                        $collectionArray[$item->getItemId()]['product']['image'] = Mage::helper('catalog/image')->init($product, 'image')
                            ->constrainOnly(true)
                            ->keepAspectRatio(true)
                            ->keepFrame(false)
                            ->resize(600, null)
                            ->__toString();

                        $collectionArray[$item->getItemId()]['product']['product_url'] = $product->getProductUrl();

                        $collectionArray[$item->getItemId()]['product']['is_configurable'] = $product->isConfigurable();

                        // Initialise the options html and config json
                        $optionhtml = $config = '';
                        if($collectionArray[$item->getItemId()]['product']['is_configurable'])
                        {
                            $blockViewType = Mage::app()->getLayout()->createBlock("Mage_Catalog_Block_Product_View_Type_Configurable");
                            // This could also be customisable to use a different Configurable Dropdown output
                            //$blockViewType = Mage::app()->getLayout()->createBlock("Folk_Outofstock_Block_Product_View_Type_Configurable");

                            $blockViewType->setProduct($product);
                            $blockViewType->setTemplate("folk/content/hotspotconfigurable.phtml");
                            $optionhtml = $this->parseHtml($blockViewType->toHtml());
                            $config = $blockViewType->getJsonConfig();
                        }

                        $collectionArray[$item->getItemId()]['product']['option_html'] = $optionhtml;
                        $collectionArray[$item->getItemId()]['product']['json_config'] = $config;
                    }
                }
            }
        }
        return $collectionArray;
    }
    public function getProductCollectionJson()
    {
        return json_encode($this->getProductCollectionArray());
    }

}