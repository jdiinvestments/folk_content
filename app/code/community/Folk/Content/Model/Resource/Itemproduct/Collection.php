<?php
class Folk_Content_Model_Resource_Itemproduct_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('folk_content/itemproduct');
    }
}