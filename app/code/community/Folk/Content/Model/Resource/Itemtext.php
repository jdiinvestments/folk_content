<?php
class Folk_Content_Model_Resource_Itemtext extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('folk_content/itemtext', 'item_text_id');
    }
}