<?php
class Folk_Content_Model_Resource_Itemproduct extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('folk_content/itemproduct', 'item_product_id');
    }
}