<?php
class Folk_Content_Model_Item extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('folk_content/item');
    }
}