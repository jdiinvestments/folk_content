<?php

class Folk_Content_Adminhtml_ItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('folk_content/item');
    }

    public function indexAction()
    {
        $this->_redirect('*/feature/*');
    }

    public function editAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $id = $this->getRequest()->getParam('id');

        try {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }

                if ($itemModel->getOverlayType() == 'text' || $itemModel->getOverlayType() == 'image') {
                    $this->_redirect('*/item/edit'.$itemModel->getOverlayType().'', array('feature' => $itemModel->getFeatureId(),'id' => $itemModel->getId()));
                } else {
                        Mage::throwException($this->__('Record has incorrect type.'));
                }
            } else {
                // Cover all routes
                Mage::throwException($this->__('No ID found.'));
            }
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/index');
        }
    }

    public function newimageAction()
    {
        $this->_forward('editimage');
    }

    public function editimageAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $imageModel = Mage::getModel('folk_content/itemimage');
        Mage::register('current_item', $itemModel);
        Mage::register('current_item_image', $imageModel);

        $id = $this->getRequest()->getParam('id');
        Mage::register('current_item_id', $id);

        try {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                if (!$imageModel->load($itemModel->getId(), 'item_id')->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
            $this->loadLayout();
            $this->_setActiveMenu('folk_content');
            $this->renderLayout();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/index');
        }
    }

    public function saveimageAction()
    {
        if ($data = $this->getRequest()->getPost())
        {
            $this->_getSession()->setFormData($data);

            // Item id
            $item_id = $this->getRequest()->getParam('id');
            $feature_id = $this->getRequest()->getParam('feature');

            // Item model
            $itemModel = Mage::getModel('folk_content/item');
            $imageModel = Mage::getModel('folk_content/itemimage');

            try
            {
                // Start processing the feature
                if ($item_id) {
                    $itemModel->load($item_id); // Load the existing model if any
                    $imageModel->load($itemModel->getId(), 'item_id'); // Load the existing model if any
                }

                // Check any images are being deleted and remove them
                if (isset($data['item'])) {
                    if (isset($data['item']['image']['delete']) && $data['item']['image']['delete'] == 1) {
                        $file = Mage::getBaseDir('media') . DS . $imageModel->getData('image');
                        if(is_file($file)) {
                            unlink($file);
                        }
                        $imageModel->setData('image', '');
                        $imageModel->save();
                    }
                }

                // First make sure there is at least an image or embed script
                if ($_FILES['item']['error']['image'] != 0 && $imageModel->getImage() == '') {

                    $this->_getSession()->addError('You must have an image for this item');
                    if ($itemModel && $itemModel->getId()) {
                        $this->_redirect('*/*/editimage', array('feature' => $feature_id, 'id' => $itemModel->getId()));
                    } else {
                        $this->_redirect('*/*/newimage', array('feature' => $feature_id));
                    }
                } else {
                    if (isset($_FILES['item'])) {
                        // See if we're deleting, if not process the image
                        if (isset($data['feature']) && isset($data['feature']['image']) && isset($data['feature']['image']['delete']) && $data['feature']['image']['delete'] == 1)
                        {
                            $file = Mage::getBaseDir('media') . DS . $model->getData('image');
                            if(is_file($file)) {
                                unlink($file);
                            }
                            $model->setData('image','');
                        }
                        else
                        {
                            if ($_FILES['item']['error']['image'] == 0) {
                                // if Image already exists for this option then delete it
                                $file = Mage::getBaseDir('media') . DS . $imageModel->getData('image');
                                if(is_file($file)) {
                                    unlink($file);
                                }
                                $imageModel->setData('image','');

                                // Once deleted process the new image
                                $imgArray = array(
                                    'name'      => $_FILES['item']['name']['image'],
                                    'type'      => $_FILES['item']['type']['image'],
                                    'tmp_name'  => $_FILES['item']['tmp_name']['image'],
                                    'error'     => $_FILES['item']['error']['image'],
                                    'size'      => $_FILES['item']['size']['image']
                                );

                                $uploader = new Varien_File_Uploader($imgArray);
                                $uploader->setAllowedExtensions(array('jpg','jpeg','png','gif'));
                                $uploader->setAllowRenameFiles(true);
                                $uploader->setFilesDispersion(false);

                                $path = Mage::helper('folk_content')-> getImagePath();

                                $name = str_replace(' ', '_', $_FILES['item']['name']['image']);

                                $uploader->save($path, $name);

                                // IMPORTANT, Get the uploaded filename, NOT correct name.
                                // Otherwise you get a bug where you can't upload an image
                                // with the same name as the existing image.
                                $image = 'folkcontent' . DS . $uploader->getUploadedFileName($name);

                                $imageModel->setData('image', $image);
                            }
                        }
                    }

                    // Add the rest of the data
                    $itemModel
                        ->setFeatureId($feature_id)
                        ->setOverlayType('image')
                        ->setLabel($data['label']);
                    $itemModel->save();

                    $imageModel
                        ->setItemId($itemModel->getId())
                        ->setLinkUrl($data['link_url']);

                    // Now save the model
                    $imageModel->save();

                    $this->_getSession()->addSuccess($this->__($itemModel->getLabel().' was successfully saved'));

                    $continue = $this->getRequest()->getParam('continue');

                    $this->_getSession()->setFormData(null); // Empty any session form data

                    if ($continue != '') {
                        $this->_redirect('*/*/editimage', array('feature' => $feature_id, 'id' => $itemModel->getId()));
                    } else {
                        $this->_redirect('*/feature/edit', array('id' => $feature_id));
                    }
                }
            }
            catch (Exception $e)
            {
                $this->_getSession()->addError($e->getMessage());
                if ($itemModel && $itemModel->getId()) {
                    $this->_redirect('*/*/editimage', array('feature' => $feature_id, 'id' => $itemModel->getId()));
                } else {
                    $this->_redirect('*/*/newimage', array('feature' => $feature_id));
                }
            }
        }
    }

    public function newtextAction()
    {
        $this->_forward('edittext');
    }

    public function edittextAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $textModel = Mage::getModel('folk_content/itemtext');
        Mage::register('current_item', $itemModel);
        Mage::register('current_item_text', $textModel);

        $id = $this->getRequest()->getParam('id');
        Mage::register('current_item_id', $id);

        try
        {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                if (!$textModel->load($itemModel->getId(), 'item_id')->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
            $this->loadLayout();
            $this->_setActiveMenu('folk_content');
            $this->renderLayout();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }
    }

    public function savetextAction()
    {
        if ($data = $this->getRequest()->getPost())
        {
            $this->_getSession()->setFormData($data);

            // Item id
            $item_id = $this->getRequest()->getParam('id');
            $feature_id = $this->getRequest()->getParam('feature');

            // Item model
            $itemModel = Mage::getModel('folk_content/item');
            $textModel = Mage::getModel('folk_content/itemtext');

            try
            {
                // Start processing the feature
                if ($item_id) {
                    $itemModel->load($item_id); // Load the existing model if any
                    $textModel->load($itemModel->getId(), 'item_id'); // Load the existing model if any
                }

                // Add the rest of the data
                $itemModel
                    ->setFeatureId($feature_id)
                    ->setOverlayType('text')
                    ->setLabel($data['label']);
                $itemModel->save();

                if ($data['colour'] == '') { $data['colour'] = '#000000'; }
                if ($data['line_height'] == '') { $data['line_height'] = 'auto'; }
                if ($data['letter_spacing'] == '') { $data['letter_spacing'] = 'normal'; }

                $textModel
                    ->setItemId($itemModel->getId())
                    ->setLinkUrl($data['link_url'])
                    ->setText($data['text'])
                    ->setFontSize($data['font_size'])
                    ->setFontFace($data['font_face'])
                    ->setColour($data['colour'])
                    ->setAlignment($data['alignment'])
                    ->setLineHeight($data['line_height'])
                    ->setLetterSpacing($data['letter_spacing'])
                    ->setBold($data['bold'])
                    ->setUnderlined($data['underlined'])
                    ->setItalic($data['italic']);

                // Now save the model
                $textModel->save();

                $this->_getSession()->addSuccess($this->__($itemModel->getLabel().' was successfully saved'));

                $continue = $this->getRequest()->getParam('continue');

                $this->_getSession()->setFormData(null); // Empty any session form data

                if ($continue != '') {
                    $this->_redirect('*/*/edittext', array('feature' => $feature_id, 'id' => $itemModel->getId()));
                } else {
                    $this->_redirect('*/feature/edit', array('id' => $feature_id));
                }
            }
            catch (Exception $e)
            {
                $this->_getSession()->addError($e->getMessage());
                if ($itemModel && $itemModel->getId()) {
                    $this->_redirect('*/*/edittext', array('feature' => $feature_id, 'id' => $itemModel->getId()));
                } else {
                    $this->_redirect('*/*/newtext', array('feature' => $feature_id));
                }
            }
        }
    }



    public function deleteAction()
    {
        // Item id
        $id = $this->getRequest()->getParam('id');
        $feature_id = null;

        // Item model
        $itemModel = Mage::getModel('folk_content/item');
        $imageModel = Mage::getModel('folk_content/itemimage');
        $textModel = Mage::getModel('folk_content/itemtext');

        try
        {
            if ($id)
            {
                if (!$itemModel->load($id)->getId())
                {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                $feature_id = $itemModel->getFeatureId();

                $name = '';
                switch ($itemModel->getOverlayType()) {
                    case 'image':
                        // Process the Image Deletion
                        if (!$imageModel->load($id, 'item_id')->getId())
                        {
                            Mage::throwException($this->__('No record with ID "%s" found.', $id));
                        }
                        // Delete the existing images, because over-bloated media folders are nasty
                        $file = Mage::getBaseDir('media') . DS . $imageModel->getData('image');
                        if(is_file($file)) { unlink($file); }

                        $name = $itemModel->getLabel();
                        $imageModel->delete();
                        $itemModel->delete();
                        break;
                    case 'text':
                        // Process the Text Deletion
                        if (!$textModel->load($id, 'item_id')->getId())
                        {
                            Mage::throwException($this->__('No record with ID "%s" found.', $id));
                        }

                        $name = $itemModel->getLabel();
                        $textModel->delete();
                        $itemModel->delete();
                        break;
                }

                $this->_getSession()->addSuccess($this->__('"%s" was successfully deleted', $name, $id));
                $this->_redirect('*/feature/edit', array('id' => $feature_id));
            }
            else {
                Mage::throwException($this->__('No id was given'));
            }
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());

            if ($itemModel && $itemModel->getId() && $itemModel->getOverlayType() == 'text') {
                $this->_redirect('*/*/edittext', array('feature' => $feature_id, 'id' => $itemModel->getId()));
            } elseif ($itemModel && $itemModel->getId() && $itemModel->getOverlayType() == 'image') {
                $this->_redirect('*/*/editimage', array('feature' => $feature_id, 'id' => $itemModel->getId()));
            } elseif ($feature_id != null) {
                $this->_redirect('*/feature/edit', array('id' => $feature_id));
            } else {
                $this->_redirect('*/feature/index');
            }
        }

    }





}
