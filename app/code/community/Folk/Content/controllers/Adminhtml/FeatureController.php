<?php

class Folk_Content_Adminhtml_FeatureController extends Mage_Adminhtml_Controller_Action
{
    /*
     * AdminHtml Permissions
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('folk_content/feature');
    }


    /*
     * A fall back page that lists out all the features when there is no return url. Shouldn't be accessible directly.
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('folk_content');
        $this->renderLayout();
    }

    /*
     * Redirects a user to the edit page
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /*
     * Opens the edit feature page
     */
    public function editAction()
    {
        // Set the return action to back (as a default)
        Mage::helper('folk_content')->setReturnAction('back');

        $model = Mage::getModel('folk_content/feature');
        Mage::register('current_feature', $model);

        $id = $this->getRequest()->getParam('id');

        try {

            if ($id) {
                if (!$model->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }

            // If a model is found for editing then set it as the return feature
            Mage::helper('folk_content')->setReturnFeatureId($id);

            $this->loadLayout();
            $this->_setActiveMenu('folk_content');
            $this->renderLayout();

        } catch (Exception $e) {

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());

            // Check the return url and redirect user
            if (Mage::helper('folk_content')->getReturnUrl()) {
                $this->_redirect(Mage::helper('folk_content')->getReturnUrl());
            } else {
                $this->_redirect('*/*/index');
            }

        }
    }

    /*
     * Saves an existing or new feature
     */
    public function saveAction()
    {
        // Check there is data to work with. If not fall out of the action and go to dashboard
        if ($data = $this->getRequest()->getPost())
        {
            $this->_getSession()->setFormData($data);

            // Feature id
            $id = $this->getRequest()->getParam('id');

            // Feature model
            $model = Mage::getModel('folk_content/feature');

            try
            {
                // Start processing the feature
                if ($id) {
                    $model->load($id); // Load the existing model if any
                }

                // Check any images are being deleted and remove them
                if (isset($data['feature'])) {
                    foreach($data['feature'] as $imgName => $imgData) {
                        if (isset($imgData['delete']) && $imgData['delete'] == 1) {
                            $file = Mage::getBaseDir('media') . DS . $model->getData($imgName);
                            if(is_file($file)) {
                                unlink($file);
                            }
                            $model->setData($imgName, '');
                            $model->save();
                        }
                    }
                }

                // First make sure there is at least an image or embed script
                if ($_FILES['feature']['error']['bg_image'] != 0 &&
                    ($model->getBgImage() == '') &&
                    $data['embed_script'] == '')
                {
                    // If there isn't a background or embed script then just fall back on the create or edit pages. No need to redirect to return url
                    $this->_getSession()->addError('You must have at least a Background image or an Embed Script');
                    if ($model && $model->getId()) {
                        $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    } else {
                        $this->_redirect('*/*/new');
                    }
                }
                else // Otherwise we're good to go
                {
                    // Check if a file has been uploaded
                    if (isset($_FILES['feature']))
                    {
                        foreach ($_FILES['feature']['name'] as $imgName => $imgFilename)
                        {
                            // See if we're deleting, if not process the image
                            if (isset($data['feature']) && isset($data['feature'][$imgName]) && isset($data['feature'][$imgName]['delete']) && $data['feature'][$imgName]['delete'] == 1)
                            {
                                $file = Mage::getBaseDir('media') . DS . $model->getData($imgName);
                                if(is_file($file)) {
                                    unlink($file);
                                }
                                $model->setData($imgName,'');
                            }
                            else
                            {
                                if ($_FILES['feature']['error'][$imgName] == 0) {
                                    // if Image already exists for this option then delete it
                                    $file = Mage::getBaseDir('media') . DS . $model->getData($imgName);
                                    if(is_file($file)) {
                                        unlink($file);
                                    }
                                    $model->setData($imgName,'');

                                    // Once deleted process the new image
                                    $imgArray = array(
                                        'name'      => $_FILES['feature']['name'][$imgName],
                                        'type'      => $_FILES['feature']['type'][$imgName],
                                        'tmp_name'  => $_FILES['feature']['tmp_name'][$imgName],
                                        'error'     => $_FILES['feature']['error'][$imgName],
                                        'size'      => $_FILES['feature']['size'][$imgName]
                                    );

                                    $uploader = new Varien_File_Uploader($imgArray);
                                    $uploader->setAllowedExtensions(array('jpg','jpeg','png','gif'));
                                    $uploader->setAllowRenameFiles(true);
                                    $uploader->setFilesDispersion(false);

                                    $path = Mage::helper('folk_content')->getImagePath();

                                    $name = str_replace(' ', '_', $_FILES['feature']['name'][$imgName]);

                                    $uploader->save($path, $name);

                                    // IMPORTANT, Get the uploaded filename, NOT correct name.
                                    // Otherwise you get a bug where you can't upload an image
                                    // with the same name as the existing image.
                                    $image = 'folkcontent' . DS . $uploader->getUploadedFileName($name);

                                    $model->setData($imgName, $image);
                                }
                            }
                        }
                    }

                    // Add the rest of the data
                    $model->setTitle($data['title'])
                        ->setEnabled($data['enabled'])
                        ->setSortOrder($data['sort_order'])
                        ->setLinkUrl($data['link_url'])
                        ->setEmbedScript($data['embed_script']);

                    // Now save the feature model
                    $model->save();

                    // Process the item overlays (if any)
                    if (!empty($data['item_overlay']))
                    {
                        $itemModel = Mage::getModel('folk_content/item');
                        $imageModel = Mage::getModel('folk_content/itemimage');
                        $textModel = Mage::getModel('folk_content/itemtext');
                        foreach ($data['item_overlay'] as $item) {
                            try
                            {
                                $itemModel->load($item['item_id']);
                                $itemModel->setCoordX($item['coord_x']);
                                $itemModel->setCoordY($item['coord_y']);
                                $itemModel->setData('z_index',$item['z_index']);
                                $itemModel->save();

                                switch($itemModel->getOverlayType()) {
                                    case 'image':
                                        $imageModel->load($itemModel->getId(), 'item_id');
                                        $imageModel->setWidth($item['image_width']);
                                        $imageModel->save();
                                        break;
                                    case 'text':
                                        $textModel->load($itemModel->getId(), 'item_id');
                                        $textModel->setWidth($item['text_width']);
                                        $textModel->save();
                                        break;
                                }
                            }
                            catch (Exception $e)
                            {
                                $this->_getSession()->addError($e->getMessage());
                            }
                        }
                    }

                    // Process the product overlays (if any)
                    if (!empty($data['item_product']))
                    {
                        // Load all the product items associated with this feature
                        $productCollection = Mage::helper('folk_content')->getFeatureItemCollection($id);
                        $productCollection->addFieldToFilter('overlay_type', array('in' => array('product')));
                        $productCollection->getSelect()
                            ->joinLeft( array('item_product' => $productCollection->getTable('folk_content/itemproduct')),
                                'main_table.item_id = item_product.item_id',
                                array( 'item_product_id', 'product_id' ) // Don't include item_id to prevent duplicate
                            );

                        $itemModel = Mage::getModel('folk_content/item');
                        $productModel = Mage::getModel('folk_content/itemproduct');

                        // Loop through all the post data for the products
                        foreach ($data['item_product'] as $productData)
                        {
                            $product_id = $productData['entity_id'];

                            // Try and get an existing item
                            $filteredProduct = $productCollection->getItemByColumnValue('product_id', $product_id);

                            // Find out if this product has been selected
                            if (!empty($productData['product_id']) && $productData['product_id'] == $product_id)
                            {
                                if (!empty($filteredProduct) && $filteredProduct->getId()) // If found then update the coordinates
                                {
                                    try
                                    {
                                        $itemModel->load($filteredProduct->getId());
                                        $itemModel->setCoordX($productData['coord_x']);
                                        $itemModel->setCoordY($productData['coord_y']);
                                        $itemModel->save();
                                    }
                                    catch (Exception $e)
                                    {
                                        $this->_getSession()->addError($e->getMessage());
                                    }
                                }
                                else // If not found then make a new link between the product and this feature
                                {
                                    $itemModel
                                        ->setFeatureId($id)
                                        ->setOverlayType('product')
                                        ->setLabel($productData['name']);
                                    $itemModel->save();

                                    $productModel
                                        ->setItemId($itemModel->getId())
                                        ->setProductId($product_id);
                                    $productModel->save();
                                }

                            }
                            else // If not selected then find out if the product is linked to this feature and remove the link
                            {
                                if (!empty($filteredProduct) && $filteredProduct->getId())
                                {
                                    try
                                    {
                                        $productModel->load($filteredProduct->getItemProductId());
                                        if ($productModel->getId()) {
                                            $itemModel->load($productModel->getItemId());
                                            $productModel->delete();
                                            $itemModel->delete();
                                        }
                                    }
                                    catch (Exception $e)
                                    {
                                        $this->_getSession()->addError($e->getMessage());
                                    }
                                }
                            }
                            $productModel->unsetData();
                            $itemModel->unsetData();
                        }
                    }

                    // Now save the feature (again, why not save twice)
                    $model->save();

                    // Little message
                    $this->_getSession()->addSuccess($this->__($model->getTitle().' was successfully saved'));

                    // Empty any session form data
                    $this->_getSession()->setFormData(null);

                    // See if the save and continue button was clicked. This redirects the user to the edit page instead
                    $continue = $this->getRequest()->getParam('continue');

                    if ($continue != '')
                    {
                        $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    }
                    else
                    {
                        // Check the return url and redirect user
                        if (Mage::helper('folk_content')->getReturnUrl())
                        {
                            // Remember to pass the data back
                            Mage::helper('folk_content')->setReturnAction('save');
                            Mage::helper('folk_content')->setReturnFeatureId($model->getId());
                            $this->_redirect(Mage::helper('folk_content')->getReturnUrl());
                        }
                        else
                        {
                            $this->_redirect('*/*/index');
                        }
                    }
                }
            }
            catch (Exception $e)
            {
                // If there is a problem fall back on the create or edit pages. No need to redirect to return url
                $this->_getSession()->addError($e->getMessage());
                if ($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/new');
                }
            }
        }
        else
        {
            $this->_getSession()->addError($this->__('No data found to save'));
            $this->_redirect('*/*');
        }
    }

    /*
     * Deletes a feature
     */
    public function deleteAction()
    {
        // Feature id
        $id = $this->getRequest()->getParam('id');

        // Feature model
        $model = Mage::getModel('folk_content/feature');

        try
        {
            if ($id)
            {
                if (!$model->load($id)->getId())
                {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }

                // Delete the existing images, because over-bloated media folders are nasty
                $file = Mage::getBaseDir('media') . DS . $model->getData('bg_image');
                if(is_file($file)) { unlink($file); }
                $file = Mage::getBaseDir('media') . DS . $model->getData('mobile_image');
                if(is_file($file)) { unlink($file); }

                $name = $model->getTitle();
                $model->delete();

                Mage::helper('folk_content')->setReturnFeatureId($id);
                $this->_getSession()->addSuccess($this->__('"%s" was successfully deleted', $name, $id));
            }
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        // Check the return url and redirect user
        if (Mage::helper('folk_content')->getReturnUrl()) {
            Mage::helper('folk_content')->setReturnAction('delete');
            $this->_redirect(Mage::helper('folk_content')->getReturnUrl());
        } else {
            $this->_redirect('*/*');
        }
    }

    /*
     * We probably shouldn't have this action
     */
    public function deleteMassAction()
    {
        $data = $this->getRequest()->getPost($this->getRequest()->getPost('massaction_prepare_key'));

        if ($data) {
            $deleted = array();

            // Feature model
            $model = Mage::getModel('folk_content/feature');

            foreach ((array) $data as $id) {
                $model->load($id);

                // Delete the existing images, because over-bloated media folders are nasty
                $file = Mage::getBaseDir('media') . DS . $model->getData('bg_image');
                if(is_file($file)) { unlink($file); }
                $file = Mage::getBaseDir('media') . DS . $model->getData('mobile_image');
                if(is_file($file)) { unlink($file); }

                $deleted[] = $model->getTitle();

                $model->delete();
            }
            $this->_getSession()->addSuccess(
                $this->__('Deleted Features: %s', implode('; ', $deleted))
            );
        }

        $this->_redirect('*/*');
    }

    public function gridAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    public function itemgridAction()
    {
        if (!Mage::registry('current_feature')) {
            Mage::register('current_feature', Mage::getModel('folk_content/feature')->load($this->getRequest()->getParam('id')));
        }

        $this->loadLayout(false);
        $this->renderLayout();
    }

    public function productgridAction()
    {
        if (!Mage::registry('current_feature')) {
            Mage::register('current_feature', Mage::getModel('folk_content/feature')->load($this->getRequest()->getParam('id')));
        }

        $this->loadLayout(false);
        $this->renderLayout();
    }


    public function itemimageeditAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $imageModel = Mage::getModel('folk_content/itemimage');
        Mage::register('current_item', $itemModel);
        Mage::register('current_item_image', $imageModel);

        $id = $this->getRequest()->getParam('id');
        Mage::register('current_item_id', $id);

        try
        {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                if (!$imageModel->load($itemModel->getId(), 'item_id')->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
            $this->loadLayout(false);
            $this->renderLayout();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }
    }


    public function itemproducteditAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $productModel = Mage::getModel('folk_content/itemproduct');
        Mage::register('current_item', $itemModel);
        Mage::register('current_item_product', $productModel);

        $id = $this->getRequest()->getParam('id');
        Mage::register('current_item_id', $id);

        try
        {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    echo ("Shite");
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                if (!$productModel->load($itemModel->getId(), 'item_id')->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
            $this->loadLayout(false);
            $this->renderLayout();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }
    }


    public function itemtexteditAction()
    {
        $itemModel = Mage::getModel('folk_content/item');
        $textModel = Mage::getModel('folk_content/itemtext');
        Mage::register('current_item', $itemModel);
        Mage::register('current_item_text', $textModel);

        $id = $this->getRequest()->getParam('id');
        Mage::register('current_item_id', $id);

        try
        {
            if ($id) {
                if (!$itemModel->load($id)->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
                if (!$textModel->load($itemModel->getId(), 'item_id')->getId()) {
                    Mage::throwException($this->__('No record with ID "%s" found.', $id));
                }
            }
            $this->loadLayout(false);
            $this->renderLayout();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }
    }
}
