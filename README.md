Folk_Content
==============

The Folk_Content module provides a drag and drop feature creation interface that can be used by other modules.

Sending the Admin User to Folk_Content
--------------

There are two entry points into the Folk_Content Module. You can send Admin Users to the **create feature** page or the **edit feature** page.

**Url to create a feature**

    $this->getUrl('*/feature/new/')

You can add this to a form layout file

    protected function _prepareLayout()
    {
        $this->setChild('add_feature_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => $this->__('Add Feature'),
                    'id'        => 'add_feature_button',
                    'onclick'   => 'window.location.href=\''.$this->getUrl('*/feature/new/').'\'',
                    'title'     => 'Add Feature'
                ))
        );
        return parent::_prepareLayout();
    }

*An example of the create link can be found in this file: app/code/community/Folk/Homepage/Block/Adminhtml/Layout/Edit/Tab/Feature.php*

**Url to edit a feature**

    $this->getUrl('*/feature/edit/', array('id' => *feature_id*))

You can apply this to a row in a grid

    public function getRowUrl($row)
    {
        return $this->getUrl('*/feature/edit/', array('id' => $row->getId()));
    }

or as an action on the grid

    $this->addColumn('action', array(
        'header' => $this->__('Action'),
        'type' => 'action',
        'getter' => 'getId',
        'actions' => array(
            array(
                'caption' => $this->__('Edit'),
                'url' => array('base' => '*/feature/edit/'),
                'field' => 'id',
            ),
        ),
    ));

*An example of the edit link can be found in this file: app/code/community/Folk/Homepage/Block/Adminhtml/Layout/Edit/Tab/Feature.php*

Returning the Admin User from Folk_Content
--------------

In Folk_Content there is a helper function that should be used to send an Admin User back to your module after a Feature is saved or deleted.

    Mage::helper('folk_content')->setReturnUrl($returnUrl);

The return url is going to be your extension's entry point. It will be a single entry point and should be able to deal with multiple actions.

In Folk_Homepage for example the entry point is the updatelayout action in the layout controller. This receives the id of layout that the feature is being added to.

    $returnUrl = '*/layout/updatelayout/id/'.$model->getId().'/';

This is set in the edit action of the layout controller because this is the point where the list of features attached to a layout will be displayed. You can set this anywhere that makes sense to your module.

*app/code/community/Folk/Homepage/controllers/Adminhtml/LayoutController.php*

Receiving data from Folk_Content
--------------

Once Folk_Content has processed the Feature it will send the Admin User back to the return url (if you provided one)

For example in Folk_Homepage this returns to an action labeled **updatelayout**

This action function must be able to handle up to 3 different actions. [back] [save] & [delete].

    Mage::helper('folk_content')->getReturnAction();

There is also a data value that is the id of the feature that was just saved or deleted.

    Mage::helper('folk_content')->getReturnFeatureId();

Finally the last thing you should always do in your return entry point action is clear the data. You can do this using this helper function

    Mage::helper('folk_content')->clearReturnData();

You can view this action in the layout controller:

    public function updatelayoutAction()
    {
        $id = $this->getRequest()->getParam('id'); // This is the id set in the return url

        $feature_id = Mage::helper('folk_content')->getReturnFeatureId();

        switch (Mage::helper('folk_content')->getReturnAction())
        {
            case 'back':
            case 'save':

                /* Add the feature to the layout */

                break;
            case 'delete':

                /* Remove the feature from the layout */

                break;
        }

        // Be responsible and clear the returnUrl
        Mage::helper('folk_content')->clearReturnData();

        // Send back to the list of features
        $this->_redirect('*/*/edit', array('id' => $id));
    }

*app/code/community/Folk/Homepage/controllers/Adminhtml/LayoutController.php*

